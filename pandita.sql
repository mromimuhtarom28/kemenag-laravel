-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Mar 2021 pada 09.31
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pandita`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_vihara`
--

CREATE TABLE `admin_vihara` (
  `id_adm_vihara` int(5) NOT NULL,
  `id_login` int(5) NOT NULL,
  `nik` int(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_vihara` int(5) NOT NULL,
  `id_profil` int(5) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_vihara`
--

INSERT INTO `admin_vihara` (`id_adm_vihara`, `id_login`, `nik`, `email`, `id_vihara`, `id_profil`, `status`, `catatan`) VALUES
(1, 9, 1234567890, 'mromimuhtarom28@gmail.com', 1, 4, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(5) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `headline` text NOT NULL,
  `isi` text NOT NULL,
  `id_login` int(5) NOT NULL,
  `tgl_terbit` datetime NOT NULL,
  `tgl_disetujui` datetime NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1,
  `gambar` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id_login` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(5) NOT NULL,
  `id_profil` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id_login`, `username`, `password`, `id_role`, `id_profil`) VALUES
(1, 'admin', '$2y$10$SJn.nNwIa3O/KqqVWitF7.xicyW8.q.lkb/3brQaI18muaPzThFtK', 1, 1),
(2, 'admin3', '$2y$10$SJn.nNwIa3O/KqqVWitF7.xicyW8.q.lkb/3brQaI18muaPzThFtK', 1, 2),
(8, 'jaya', '$2y$10$P0P58sqw3zf1rJGROVR3v.HFleO7HFdH2aWAhv7oAtiGOq/FgQmy2', 2, 3),
(9, 'admin7', '$2y$10$93locHLNrEg2/BjqS4yWAe7EY7XinhhOGZFFVtW7qTmalf/MI0YY6', 3, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pandita`
--

CREATE TABLE `pandita` (
  `id_pandita` int(5) NOT NULL,
  `no_ktp` int(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_profil` int(5) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `id_vihara` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pandita`
--

INSERT INTO `pandita` (`id_pandita`, `no_ktp`, `email`, `id_profil`, `status`, `id_vihara`) VALUES
(3, 2147483647, 'BakuHantamCrew1@gmail.com', 3, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pernikahan`
--

CREATE TABLE `pernikahan` (
  `no_nikah` varchar(25) NOT NULL,
  `id_vihara` int(5) NOT NULL,
  `id_pandita` int(5) NOT NULL,
  `id_login` int(5) NOT NULL,
  `nama_suami` varchar(255) NOT NULL,
  `nik_suami` int(20) NOT NULL,
  `tgl_lahir_suami` date NOT NULL,
  `nama_istri` varchar(255) NOT NULL,
  `nik_istri` int(20) NOT NULL,
  `tgl_lahir_istri` date NOT NULL,
  `tgl_nikah` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pernikahan`
--

INSERT INTO `pernikahan` (`no_nikah`, `id_vihara`, `id_pandita`, `id_login`, `nama_suami`, `nik_suami`, `tgl_lahir_suami`, `nama_istri`, `nik_istri`, `tgl_lahir_istri`, `tgl_nikah`) VALUES
('3242', 1, 3, 9, 'Winata', 353453, '2021-03-01', 'Willy', 34543, '2018-11-01', '2021-02-03 05:21:05'),
('4234', 1, 3, 9, 'teku', 23456, '1988-06-08', 'teku', 2342, '1998-11-20', '2021-03-09 02:33:00'),
('4331', 1, 3, 9, 'Wira', 54353, '2020-09-01', 'windi', 3534, '2019-08-01', '2021-03-08 05:29:31'),
('defsedf', 1, 3, 9, 'Cahyadi', 2123223, '2020-03-03', 'Cai cu mei', 35654363, '2015-06-12', '2017-03-08 05:19:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_login`
--

CREATE TABLE `profil_login` (
  `id_profil` int(5) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `ttl` text NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_login`
--

INSERT INTO `profil_login` (`id_profil`, `nama_lengkap`, `alamat`, `ttl`, `telp`) VALUES
(1, 'Admin', 'admin', '', '082392191962'),
(2, 'Wan Halim', 'tiban', '1998-11-20', '082392191962'),
(3, 'jaya', 'Aceh', '2020-11-28', '083833571194'),
(4, 'Rowam toa', 'Cipta Puri mas', '2020-11-12', '082392191962');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id_role` int(5) NOT NULL,
  `nama_role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'admin kemenag'),
(2, 'pandita'),
(3, 'admin vihara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `vihara`
--

CREATE TABLE `vihara` (
  `id_vihara` varchar(25) NOT NULL,
  `nama_vihara` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `vihara`
--

INSERT INTO `vihara` (`id_vihara`, `nama_vihara`, `alamat`, `status`) VALUES
('1', 'Moghalana', 'Baloi', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin_vihara`
--
ALTER TABLE `admin_vihara`
  ADD PRIMARY KEY (`id_adm_vihara`);

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indeks untuk tabel `pandita`
--
ALTER TABLE `pandita`
  ADD PRIMARY KEY (`id_pandita`);

--
-- Indeks untuk tabel `pernikahan`
--
ALTER TABLE `pernikahan`
  ADD PRIMARY KEY (`no_nikah`);

--
-- Indeks untuk tabel `profil_login`
--
ALTER TABLE `profil_login`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `vihara`
--
ALTER TABLE `vihara`
  ADD PRIMARY KEY (`id_vihara`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin_vihara`
--
ALTER TABLE `admin_vihara`
  MODIFY `id_adm_vihara` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pandita`
--
ALTER TABLE `pandita`
  MODIFY `id_pandita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `profil_login`
--
ALTER TABLE `profil_login`
  MODIFY `id_profil` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
