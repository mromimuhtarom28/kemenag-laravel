@php 
$idrole = Session::get('id_role');

$countrequest = App\Models\Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita')
                      ->where('type', 1)
                      ->count();

$countditolak = App\Models\Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita')
                      ->where('type', 0)
                      ->count();
@endphp
<style>
.badge {
  border-radius: 50%;
  background-color: red;
  color: white;
}
</style>
{{-- <div class="sb-sidenav-menu">
    <div class="nav"> --}}
        @if($idrole == 3)
            @php 
                $idlogin = Session::get('idlogin');
                $admvhr = App\Models\AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                        ->join('login', 'admin_vihara.id_login', 'login.id_login')
                        ->where('admin_vihara.id_login', $idlogin)
                        ->first();
            @endphp
            @if($admvhr->status != 0)
                <a class="nav-link" href="{{ route('Dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <a class="nav-link" href="{{ route('Pernikahan') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                    Perkawinan
                </a>
                <a class="nav-link" href="{{ route('List-Ditolak-Disdukcapil') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                    List Ditolak Disdukcapil @if($countditolak != 0) <span class="badge">{{ $countditolak }}</span> @endif
                </a>
                <a class="nav-link" href="{{ route('Admin') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                    Admin
                </a>
                @if($idrole == 1)
                <a class="nav-link" href="{{ route('Vihara') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-vihara"></i></div>
                    Vihara
                </a>
                @endif
            @endif
            <a class="nav-link" href="{{ route('Profile') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                Profile
            </a>
        @elseif($idrole == 4)
            <a class="nav-link" href="{{ route('Dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard
            </a>
            <a class="nav-link" href="{{ route('Pernikahan') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                Perkawinan
            </a>
            <a class="nav-link" href="{{ route('Request-AktePerkawinan') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                Request Akte Perkawinan @if($countrequest != 0) <span class="badge">{{ $countrequest }}</span> @endif
            </a>
            <a class="nav-link" href="{{ route('Disdukcapil') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                Admin Disdukcapil
            </a>
            <a class="nav-link" href="{{ route('Profile') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                Profile
            </a>

        @else
            <a class="nav-link" href="{{ route('Dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard
            </a>
            <a class="nav-link" href="{{ route('Pernikahan') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                Perkawinan
            </a>
            
            @if($idrole == 2)
            <a class="nav-link" href="{{ route('List-Ditolak-Disdukcapil') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-user-alt"></i></div>
                List Ditolak Disdukcapil @if($countditolak != 0) <span class="badge">{{ $countditolak }}</span> @endif
            </a>
            @endif
            <a class="nav-link" href="{{ route('Admin') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                Admin
            </a>
            @if($idrole == 1)
            <a class="nav-link" href="{{ route('Disdukcapil') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                Admin Disdukcapil
            </a>
            <a class="nav-link" href="{{ route('Vihara') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-vihara"></i></div>
                Vihara
            </a>
            @endif
        @endif
        <a class="nav-link" href="{{ route('logout-process') }}">
            <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
            Keluar
        </a>
    {{-- </div>
</div> --}}
