@extends('index')

@section('content')
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Request Akte Pernikahan
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No Surat Keterangan Nikah</th>
                        <th>NIK Suami</th>
                        <th>Nama Suami</th>
                        <th>Tanggal Lahir Suami</th>
                        <th>KK Suami</th>
                        <th>Akte Perceraian Suami</th>
                        <th>Akte Kematian Istri</th>
                        <th>Surat Ijin TNI/POLRI Suami</th>
                        <th>NIK Istri</th>
                        <th>Nama Istri</th>
                        <th>Tanggal Lahir Istri</th>
                        <th>KK Istri</th>
                        <th>Akte Perceraian Istri</th>
                        <th>Akte Kematian Suami</th>
                        <th>Surat Ijin TNI/POLRI Istri</th>
                        <th>Nama pandita yang nikahkan</th>
                        <th>Tanggal Nikah</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($nikah as $nk)
                    <tr>
                        <td><a href="#" data-toggle="modal" class="bukunikahimg" data-target="#bukunikah" data-nama="{{ $nk->nama_suami }}" data-statusorg="suratketerangannikah" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->no_nikah }}</a></td>
                        <td><a href="#" data-toggle="modal" class="ktpimg" data-target="#nikktp" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nik_suami }}</a></td>
                        <td><a href="#" data-toggle="modal" class="foto3x4" data-target="#foto3x4" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nama_suami }}</a></td>
                        <td><a href="#" data-toggle="modal" class="aktekelahiran" data-target="#aktekelahiran" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->tgl_lahir_suami }}</a></td>
                        <th>
                            {{-- @if(file_exists(public_path().'/pernikahan/suami/kk/'.$nk->id_nikah.'.jpg')) --}}
                                <a href="#" data-toggle="modal" class="kk btn btn-primary" data-target="#kk" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            {{-- @endif --}}
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/akteperceraian/'.$nk->id_nikah.'.jpg'))
                            <a href="#" data-toggle="modal" class="akteperceraian btn btn-primary" data-target="#akteperceraian" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/aktekematian/'.$nk->id_nikah.'.jpg'))
                            <a href="#"  data-toggle="modal" class="aktekematian btn btn-primary" data-target="#aktekematian" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/suratijintnipolri/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="suratijintnipolri btn btn-primary" data-target="#suratijintnipolri" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <td><a href="#" data-toggle="modal" class="ktpimg" data-target="#nikktp" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nik_istri }}</a></td>
                        <td><a href="#" data-toggle="modal" class="foto3x4" data-target="#foto3x4" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nama_istri }}</a></td>
                        <td><a href="#" data-toggle="modal" class="aktekelahiran" data-target="#aktekelahiran" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->tgl_lahir_istri }}</a></td>
                        <th>
                            {{-- @if(file_exists(public_path().'/pernikahan/istri/kk/'.$nk->id_nikah.'.jpg')) --}}
                            <a href="#" data-toggle="modal" class="kk btn btn-primary" data-target="#kk" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            {{-- @endif --}}
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/akteperceraian/'.$nk->id_nikah.'.jpg'))
                            <a href="#"  data-toggle="modal" class="akteperceraian btn btn-primary" data-target="#akteperceraian" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/aktekematian/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="aktekematian btn btn-primary" data-target="#aktekematian" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/suratijintnipolri/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="suratijintnipolri btn btn-primary" data-target="#suratijintnipolri" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <td>{{ $nk->pandita->ProfileLogin->nama_lengkap }}</td>
                        <td>{{ $nk->tgl_nikah }}</td>
                        <td><a href="#" class="btn btn-success btn-approve" data-toggle="modal" data-idnikah="{{ $nk->id_nikah }}" data-target="#approve">Terima</a></td>
                        <td><a href="#" class="btn btn-danger btn-decline" data-toggle="modal" data-target="#decline" data-idnikah="{{ $nk->id_nikah }}">Ditolak</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $nikah->links('pagination.default') }}</div>
        </div>
    </div>
</div> 

<div class="modal fade" id="approve" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menerima Data Ini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Request-AktePerkawinan-Approve') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="approve-pk" name="pk">
                    <div style="width:100%;font-weight:bold;" align="center"> Anda yakin ingin menerima data ini?</div><br>
                    <span>Akte Perkawinan</span><br>
                    <input type="file" accept="image/jpeg" name="aktePernikahan" required><br>           
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Diterima</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="decline" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menolak Data Ini</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Request-AktePerkawinan-Decline') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="decline-pk" name="pk">
                    <div style="width:100%;font-weight:bold;" align="center"> Anda yakin ingin menolak data ini?</div><br>
                    <textarea name="catatan" placeholder="Catatan" cols="30" rows="10" class="form-control" required></textarea>         
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ditolak</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="nikktp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto KTP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgktp" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="foto3x4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgfoto3x4" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="aktekelahiran" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Kelahiran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgaktekelahiran" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="suratijintnipolri" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Surat Ijin Nikah dari TNI / POLRI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgsuratijintnipolri" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="akteperceraian" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Perceraian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgakteperceraian" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="aktekematian" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Kematian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgaktekematian" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto KK</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgkk" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="detailcatatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Catatan dari disdukcapil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <textarea id="catatantxt" cols="30" rows="10" class="form-control" readonly></textarea>                      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>



<div class="modal fade" id="bukunikah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Buku Nikah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgbukunikah" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>




<script>
    $('.btn-approve').click(function(){
        var idnikah = $(this).attr('data-idnikah');
        $('#approve-pk').val(idnikah);
    })

    $('.btn-decline').click(function(){
        var idnikah = $(this).attr('data-idnikah');
        $('#decline-pk').val(idnikah);
    })
    
    $('.catatanmodal').click(function(){
        var catatan = $(this).attr('data-catatan');
        $('#catatantxt').val(catatan);
    });

    $('.ktpimg').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/ktp/'+idnikah+'.jpg'
        $('#imgktp').attr("src", srcimg);
    });

    $('.aktekematian').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/aktekematian/'+idnikah+'.jpg'
        $('#imgaktekematian').attr("src", srcimg);
    });

    $('.akteperceraian').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/akteperceraian/'+idnikah+'.jpg'
        $('#imgakteperceraian').attr("src", srcimg);
    });

    $('.suratijintnipolri').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/suratijintnipolri/'+idnikah+'.jpg'
        $('#imgakteperceraian').attr("src", srcimg);
    });

    $('.foto3x4').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/foto3x4/'+idnikah+'.jpg'
        $('#imgfoto3x4').attr("src", srcimg);
    });

    $('.aktekelahiran').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/aktekelahiran/'+idnikah+'.jpg'
        $('#imgfoto3x4').attr("src", srcimg);
    });

    $('.kk').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/kk/'+idnikah+'.jpg'
        $('#imgkk').attr("src", srcimg);
    });


    $('.bukunikahimg').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/'+idnikah+'.jpg'
        $('#imgbukunikah').attr("src", srcimg);
    });
    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection