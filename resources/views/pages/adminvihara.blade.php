@extends('index')

@section('content')
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imginsert').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }

    function readURLeditimg(input, idlogin) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgedit'+idlogin).attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Akun Admin
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <table width="100%">
                    <tr>
                        <td align="left"><a href="#" data-toggle="modal" data-target="#createadmin" class="btn btn-primary">Membuat Akun Admin</a></td>
                        <td align="right">
                            <form action="{{ route('Admin') }}">
                            <table>
                                <tr>
                                    <td><input type="text" name="username" class="form-control" placeholder="Nama Pengguna / NIK" @if(isset($username)) value="{{ $username }}" @endif></td>
                                    <td><button type="submit" class="btn btn-primary">Cari</button></td>
                                </tr>
                            </table>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Gambar Profil</th>
                        <th>NIK</th>
                        <th>Nama Pengguna</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>Tanggal Lahir</th>
                        <th>Telp</th>
                        <th>Status</th>
                        <th>Ganti Kata Sandi</th>
                        <th>Catatan</th>
                        @if($id_role == 1)
                        <th></th>
                        @else
                        <th>Hapus</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($admin as $adm)
                        <tr>
                            <td align="center" class="kolomeditgambar{{ $adm->id_login }}">
                                <div class="gambarbelumedit{{ $adm->id_login }}"  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                    <img class="gambarbelumedit{{ $adm->id_login }}" src="/upload/profile/user/{{ $adm->id_login }}.jpg" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                                </div>
                                <br class="gambarbelumedit{{ $adm->id_login}}">
                                <a href="#" class="btn btn-primary ubahgambar{{ $adm->id_login}}">Ubah Gambar</a> 
                            </td>
                            <td>{{ $adm->nik }}</td>
                            <td>{{ $adm->username }}</td>
                            <td>{{ $adm->email }}</td>
                            <td>{{ $adm->alamat }}</td>
                            <td>{{ $adm->ttl }}</td>
                            <td>{{ $adm->telp }}</td>
                            <td>
                                @if($adm->status == 1)
                                <span class="btn" style="background-color:#ffc107;color:#fff;font-weight:regular;cursor: default;">Menunggu Diterima dari KEMENAG</span>
                                @elseif($adm->status == 2)
                                <span class="btn" style="background-color:#198754;color:#fff;font-weight:regular;cursor: default;">Diterima KEMENAG</span>
                                @elseif($adm->status == 0)
                                <span class="btn" style="background-color:#dc3545;color:#fff;font-weight:regular;cursor: default;">Menunggu pengguna menginput ulang</span>
                                @endif
                            </td>
                            <td><a href="#" data-pk="{{ $adm->id_login }}" data-toggle="modal" data-target="#updatepwd" class="btn btn-primary changepwd">Ganti Kata Sandi</a></td>
                            <td>{{ $adm->catatan }}</td>
                            @if($id_role == 1)
                                @if($adm->status == 1)
                                    <td style="display:flex;">
                                        <a href="#" data-toggle="modal" data-target="#ApproveAccount" data-username="{{ $adm->username }}" data-pk="{{ $adm->id_adm_vihara }}" class="btn btn-success accept-btn">Diterima</a>
                                        <a href="#" data-toggle="modal" data-target="#DeclineAccount" data-username="{{ $adm->username }}" data-pk="{{ $adm->id_adm_vihara }}" class="btn btn-danger decline-btn">Ditolak</a>
                                    </td>
                                @elseif($adm->status == 2) 
                                    <td colspan="2"><span style="color:green">Akun Diterima</span></td>
                                @elseif($adm->status == 0)
                                     <td colspan="2"><span class="btn btn-warning" style="cursor:default;color:#fff;">Menunggu Pengguna mengisi kembali</span></td>
                                @endif
                            @else 
                            <td><a href="#" data-pk="{{ $adm->id_login }}" data-toggle="modal" data-target="#deleteakun" class="deleteaccount" style="color:red;font-weight:bold">&times;</a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $admin->links('pagination.default') }}</div>
        </div>
    </div>
</div> 


{{-- insert data --}}
<div class="modal fade" id="createadmin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Akun Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-processInsert') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <div style="border-radius:10px;border:1px solid black;background-color:#cccccc;width:200px;height:100px;position: relative;display: inline-block;">
                                    <img src="/admin/assets/img/uploadimg.png" alt="" id="imginsert" max-width="100px" height="98px">
                                </div><br>                                    
                                <input type="file" accept="image/jpg" name="file" id="btnimginsert" onchange="readURL(this);" required>
                            </td>
                        </tr>
                    </table><br>
                    <div class="breadcrumb" align="left">
                        Catatan : <br>
                        - untuk kata sandi di ambil berdasarkan nik <br>
                        - nama pengguna tidak boleh ada spasi dan tidak boleh melebihi 50 karakter
                    </div>
                    <input type="number" name="nik" placeholder="NIK" class="form-control" required><br>
                    <input type="text" name="namaLengkap" placeholder="Nama Lengkap" class="form-control" required><br>
                    <input type="text" name="namaPengguna" placeholder="Nama Pengguna" class="form-control" required><br>    
                    <input type="text" name="email" placeholder="Email" class="form-control" required><br>        
                    <input type="number" name="noTelp" placeholder="No Telp atau No HP" class="form-control" required><br>
                    {{-- <input type="text" name="tempatLahir" class="form-control" placeholder="Tempat Lahir" required><br> --}}
                    <input type="date" name="tanggalLahir" placeholder="Tanggal Lahir" class="form-control" required><br>   
                    <textarea name="alamat" class="form-control" cols="30" rows="10" placeholder="Alamat" required></textarea>                  
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Delete Data --}}
<div class="modal fade" id="deleteakun" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Akun Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-delete') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    Apakah anda yakin ingin menghapus akun ini ? <br>
                    <input type="hidden" name="pk" id="pk-destroy">
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Hapus Akun</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Update password --}}
<div class="modal fade" id="updatepwd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Mengubah Kata sandi admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-updatepwd') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <div class="breadcrumb" align="left">
                        Catatan : <br>
                        - untuk kata sandi yang sedang login adalah dari akun yang lagi login sekarang
                    </div>
                    <input type="hidden" name="pk" id="pk-pwd">
                    <input type="password" name="kataSandiBaru" placeholder="Kata Sandi Baru" class="form-control" required><br>
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ubah Kata Sandi</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Approve Akun --}}
<div class="modal fade" id="ApproveAccount" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menerima Akun Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-Approve') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <input type="hidden" name="pk" id="pkapprove">
                    Apakah anda yakin ingin menerima akun dengan nama pengguna <b class="usernameapp"></b>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Yakin</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Decline Akun --}}
<div class="modal fade" id="DeclineAccount" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menolak Akun Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-Decline') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <input type="hidden" name="pk" id="pkdecline">
                    Apakah anda yakin ingin menolak akun dengan nama pengguna <b class="usernamedec"></b><br>
                    berikan alasan untuk di tolak
                    <textarea name="catatan" class="form-control" cols="30" rows="10" placeholder="Alasan" required></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Yakin</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>




<script>
    $('.changepwd').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-pwd').val(pk);
    });

    $('.accept-btn').click(function(){
        var pk = $(this).attr('data-pk');
        var username = $(this).attr('data-username');
        console.log(username);
        $('.usernamemodalcontent').remove();
        $('#pkapprove').val(pk);
        $('.usernameapp').append('<span class="usernamemodalcontent">'+username+'</span>');
    });

    $('.decline-btn').click(function(){
        var pk = $(this).attr('data-pk');
        var username = $(this).attr('data-username');
        console.log(username);
        $('.usernamemodalcontent').remove();
        $('#pkdecline').val(pk);
        $('.usernamedec').append('<span class="usernamemodalcontent">'+username+'</span>');
    });

    $('.deleteaccount').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-destroy').val(pk);
    })

    @foreach($admin as $adm)
        $(".ubahgambar{{ $adm->id_login }}").on("click", function() {
            console.log('asdf');
            $(this).hide();
            $(".gambarbelumedit{{ $adm->id_login }}").css("display", "none");
            $(".kolomeditgambar{{ $adm->id_login }}").append('<form method="POST" action="{{ route("Admin-ImgChange")}}" enctype="multipart/form-data" class="editgambar{{ $adm->id_login}}"> @csrf <div style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;"><img src="/upload/profile/user/{{ $adm->id_login }}.jpg" id="imgedit{{ $adm->id_login }}" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;"></div><br><input type="hidden" name="pk" value="{{ $adm->id_login }}"><input type="file" onchange="readURLeditimg(this,{{ $adm->id_login }})" class="btn btn-secondary" accept="image/jpeg" name="file" id="editgambar" required><br><button type="submit" class="btn btn-success">Simpan</button> <a href="#" class="btn btn-danger editgambar" id="bataleditgambar{{ $adm->id_login}}">Batal</a></form>');
        });

        $('.kolomeditgambar{{ $adm->id_login}}').on('click', '#bataleditgambar{{ $adm->id_login }}', function(){
            $(".ubahgambar{{ $adm->id_login}}").show();
            $(".gambarbelumedit{{ $adm->id_login}}").css("display", "block");
            $(".editgambar{{ $adm->id_login}}").remove();
        });
    @endforeach
    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection