@extends('index')

@section('content')
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data Ditolak Disdukcapil
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No Surat Keterangan Nikah</th>
                        <th>NIK Suami</th>
                        <th>Nama Suami</th>
                        <th>Tanggal Lahir Suami</th>
                        <th>KK Suami</th>
                        <th>Akte Perceraian Suami</th>
                        <th>Akte Kematian Istri</th>
                        <th>Surat Ijin TNI/POLRI Suami</th>
                        <th>NIK Istri</th>
                        <th>Nama Istri</th>
                        <th>Tanggal Lahir Istri</th>
                        <th>KK Istri</th>
                        <th>Akte Perceraian Istri</th>
                        <th>Akte Kematian Suami</th>
                        <th>Surat Ijin TNI/POLRI Istri</th>
                        <th>Nama pandita yang nikahkan</th>
                        <th>Tanggal Nikah</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($nikah as $nk)
                    <tr>
                        <td><a href="#" data-toggle="modal" class="bukunikahimg" data-target="#bukunikah" data-nama="{{ $nk->nama_suami }}" data-statusorg="suratketerangannikah" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->no_nikah }}</a></td>
                        <td><a href="#" data-toggle="modal" class="ktpimg" data-target="#nikktp" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nik_suami }}</a></td>
                        <td><a href="#" data-toggle="modal" class="foto3x4" data-target="#foto3x4" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nama_suami }}</a></td>
                        <td><a href="#" data-toggle="modal" class="aktekelahiran" data-target="#aktekelahiran" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->tgl_lahir_suami }}</a></td>
                        <th>
                            {{-- @if(file_exists(public_path().'/pernikahan/suami/kk/'.$nk->id_nikah.'.jpg')) --}}
                                <a href="#" data-toggle="modal" class="kk btn btn-primary" data-target="#kk" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            {{-- @endif --}}
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/akteperceraian/'.$nk->id_nikah.'.jpg'))
                            <a href="#" data-toggle="modal" class="akteperceraian btn btn-primary" data-target="#akteperceraian" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/aktekematian/'.$nk->id_nikah.'.jpg'))
                            <a href="#"  data-toggle="modal" class="aktekematian btn btn-primary" data-target="#aktekematian" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/suami/suratijintnipolri/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="suratijintnipolri btn btn-primary" data-target="#suratijintnipolri" data-nama="{{ $nk->nama_suami }}" data-statusorg="suami" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <td><a href="#" data-toggle="modal" class="ktpimg" data-target="#nikktp" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nik_istri }}</a></td>
                        <td><a href="#" data-toggle="modal" class="foto3x4" data-target="#foto3x4" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->nama_istri }}</a></td>
                        <td><a href="#" data-toggle="modal" class="aktekelahiran" data-target="#aktekelahiran" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">{{ $nk->tgl_lahir_istri }}</a></td>
                        <th>
                            {{-- @if(file_exists(public_path().'/pernikahan/istri/kk/'.$nk->id_nikah.'.jpg')) --}}
                            <a href="#" data-toggle="modal" class="kk btn btn-primary" data-target="#kk" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            {{-- @endif --}}
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/akteperceraian/'.$nk->id_nikah.'.jpg'))
                            <a href="#"  data-toggle="modal" class="akteperceraian btn btn-primary" data-target="#akteperceraian" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/aktekematian/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="aktekematian btn btn-primary" data-target="#aktekematian" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <th>
                            @if(file_exists(public_path().'/pernikahan/istri/suratijintnipolri/'.$nk->id_nikah.'.jpg'))
                                <a href="#"  data-toggle="modal" class="suratijintnipolri btn btn-primary" data-target="#suratijintnipolri" data-nama="{{ $nk->nama_suami }}" data-statusorg="istri" data-idnikah="{{ $nk->id_nikah }}">Lihat Detail</a>
                            @endif
                        </th>
                        <td>{{ $nk->pandita->ProfileLogin->nama_lengkap }}</td>
                        <td>{{ $nk->tgl_nikah }}</td>
                        <td><a href="#" class="btn btn-danger btn-edit" data-catatan="{{ $nk->catatan }}" data-time="{{ date('H:i:s', strtotime($nk->tgl_nikah)) }}" data-idnikah="{{ $nk->id_nikah }}" data-nonikah="{{ $nk->no_nikah }}" data-niksuami="{{ $nk->nik_suami }}" data-namasuami="{{ $nk->nama_suami }}" data-tgllhrsuami="{{ $nk->tgl_lahir_suami }}" data-nikistri="{{ $nk->nik_istri }}" data-namaistri="{{ $nk->nama_istri }}" data-tgllhristri="{{ $nk->tgl_lahir_istri }}" data-idpandita="{{ $nk->id_pandita }}" data-tglnikah="{{ date('Y-m-d', strtotime($nk->tgl_nikah)) }}" data-idvihara="{{ $nk->id_vihara }}" data-target="#editmodal" data-toggle="modal">Ubah data</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $nikah->links('pagination.default') }}</div>
        </div>
    </div>
</div> 


<div class="modal fade" id="nikktp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto KTP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgktp" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="foto3x4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgfoto3x4" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="aktekelahiran" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Kelahiran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgaktekelahiran" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="suratijintnipolri" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Surat Ijin Nikah dari TNI / POLRI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgsuratijintnipolri" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="akteperceraian" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Perceraian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgakteperceraian" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('List-Ditolak-Disdukcapil-Update')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id_nikah" id="idnikah">
                    <div class="alert alert-danger" role="alert">
                        Catatan Disdukcapil :
                        <div class="catatandisdukcapil">

                        </div>
                    </div>
                    <span>Foto Surat Keterangan Kawin</span><br>
                    <input type="file" accept="image/jpeg" name="suratKeteranganKawin"><br><br>
                    <span>No Surat Keterangan Kawin</span><br>
                    <input type="text" name="noKeteranganKawin" id="nonikah" placeholder="Nomor Nikah" class="form-control"><br>
                    <span>Nama Pandita</span><br>
                    <select name="namaPandita" id="idpandita" class="form-control">
                        <option value="">Pilih Pandita</option>
                    </select><br>
                    <span>Tanggal Kawin</span><br>
                    <table width="100%">
                        <tr>
                            <td>
                                <input type="date" name="tanggalNikah" id="tglnikahedit" placeholder="Tanggal Nikah" class="form-control" required>
                            </td>
                            <td>
                                <input type="time" name="jamNikah" id="jamNikah" placeholder="Jam Nikah" class="form-control" required>
                            </td>
                        </tr>
                    </table>
                    <div align="center" style="width:100% !important;">Riwayat Suami</div><br>
                    <span>Nik Suami</span><br>
                    <input type="text" name="nikSuami" id="niksuami" placeholder="NIK Suami" class="form-control"><br>
                    <span>KK Suami</span><br>
                    <input type="file" accept="image/jpeg" name="kkSuami"><br><br>
                    <span>Foto Suami</span><br>
                    <input type="file" accept="image/jpeg" name="fotoSuami3x4"><br><br>
                    <span>Akte Kelahiran Suami</span><br>
                    <input type="file" accept="image/jpeg" name="akteKelahiranSuami"><br><br>
                    <span>Akte Perceraian Suami</span><br>
                    <input type="file" accept="image/jpeg" name="fotoAktePerceraianSuami"><br><br>
                    <span>Akte Kematian Istri</span><br>
                    <input type="file" accept="image/jpeg" name="fotoAkteKematianIstri" i><br><br>
                    <span>Surat Ijin TNI / POLRI Suami</span><br>
                    <input type="file" accept="image/jpeg" name="suratIjinTniPolriSuami" i><br><br>
                    <span>Nama Suami</span><br>
                    <input type="text" name="namaSuami" id="namasuami" placeholder="Nama Suami" class="form-control"><br>
                    {{-- <input type="text" name="tempatLahir" placeholder="Tempat Lahir" class="form-control"><br> --}}
                    <span>Tanggal Lahir Suami</span><br>
                    <input type="date" name="tanggalLahirSuami" id="tgllhrsuami" placeholder="Tanggal Lahir Suami" class="form-control"><br>
                    <span>KTP Suami</span><br>
                    <input type="file" accept="image/jpeg" name="ktpSuami"><br><br>
                    <div align="center" style="width:100%;">Riwayat Istri</div><br>
                    <span>NIK Istri</span><br>
                    <input type="text" name="nikIstri" id="nikistri" placeholder="NIK Istri" class="form-control"><br>
                    <span>KK Istri</span><br>
                    <input type="file" accept="image/jpeg" name="kkIstri"><br><br>
                    <span>Foto Istri</span><br>
                    <input type="file" accept="image/jpeg" name="fotoIstri3x4"><br><br>
                    <span>Akte Kelahiran Istri</span><br>
                    <input type="file" accept="image/jpeg" name="fotoAkteKelahiranIstri"><br><br>
                    <span>Akte Perceraian Istri</span><br>
                    <input type="file" accept="image/jpeg" name="fotoAktePerceraianIstri"><br><br>
                    <span>Akte Kematian Suami</span><br>
                    <input type="file" accept="image/jpeg" name="fotoAkteKematianSuami" i><br><br>
                    <span>Surat Ijin TNI / POLRI Istri</span><br>
                    <input type="file" accept="image/jpeg" name="suratIjinTniPolriIstri" i><br><br>
                    {{-- <input type="text" name="tempatLahir" placeholder="Tempat Lahir" class="form-control"><br> --}}
                    <span>Nama Istri</span><br>
                    <input type="text" name="namaIstri" id="namaistri" placeholder="Nama Istri" class="form-control"><br>
                    <span>Tanggal Lahir Istri</span><br>
                    <input type="date" name="tanggalLahirIstri" id="tgllhristri" placeholder="Tanggal Lahir Istri" class="form-control"><br>
                    <span>KTP Istri</span><br>
                    <input type="file" accept="image/jpeg" name="ktpIstri">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Edit Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="aktekematian" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Akte Kematian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgaktekematian" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto KK</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgkk" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>


<div class="modal fade" id="detailcatatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Catatan dari disdukcapil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <textarea id="catatantxt" cols="30" rows="10" class="form-control" readonly></textarea>                      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>



<div class="modal fade" id="bukunikah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Foto Buku Nikah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" align="center">
                    <img src="" id="imgbukunikah" width="100%" max-height="5000">                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
        </div>
    </div>
</div>




<script>
    $('.btn-approve').click(function(){
        var idnikah = $(this).attr('data-idnikah');
        $('#approve-pk').val(idnikah);
    })

    $('.btn-edit').click(function(){
        var nonikah     = $(this).attr('data-nonikah');
        var niksuami    = $(this).attr('data-niksuami');
        var namasuami   = $(this).attr('data-namasuami');
        var tgllhrsuami = $(this).attr('data-tgllhrsuami');
        var nikistri    = $(this).attr('data-nikistri');
        var namaistri   = $(this).attr('data-namaistri');
        var tgllhristri = $(this).attr('data-tgllhristri');
        var idpandita   = $(this).attr('data-idpandita');
        var tglnikah    = $(this).attr('data-tglnikah');
        var jamnikah    = $(this).attr('data-time')
        var idvihara    = $(this).attr('data-idvihara');
        var idnikah     = $(this).attr('data-idnikah');
        var catatan     = $(this).attr('data-catatan');
        $('#vihara').val(idvihara);
        $('#idnikah').val(idnikah);
        $('#nonikah').val(nonikah);
        $('#niksuami').val(niksuami);
        $('#namasuami').val(namasuami);
        $('#tgllhrsuami').val(tgllhrsuami);
        $('#nikistri').val(nikistri);
        $('#namaistri').val(namaistri);
        $('#tgllhristri').val(tgllhristri);
        $('#tglnikahedit').val(tglnikah);
        $('#jamNikah').val(jamnikah);
        $('.isicatatan').remove();
        $('.catatandisdukcapil').append('<div class="isicatatan">'+catatan+'</div>');
        $('.panditaopt').remove();
        $.ajax({
            url: '{{ route("list-pandita") }}',
            type: 'post',
            data:{
                idvihara:idvihara,
                _token: "{{ csrf_token() }}"
            },
            success: function(response){
                //add response in modal body
                var obj = response;
                $.each( obj.pandita, function(index, pndt ) {
                    if(idpandita == pndt.id_pandita){
                        $('#idpandita').append('<option value="'+pndt.id_pandita+'" selected class="panditaopt">'+pndt.nama_lengkap+'</option>');
                    } else {
                        $('#idpandita').append('<option value="'+pndt.id_pandita+'" class="panditaopt">'+pndt.nama_lengkap+'</option>');
                    }
                    
                });

            }
        });


    })

    $('.btn-decline').click(function(){
        var idnikah = $(this).attr('data-idnikah');
        $('#decline-pk').val(idnikah);
    })
    
    $('.catatanmodal').click(function(){
        var catatan = $(this).attr('data-catatan');
        $('#catatantxt').val(catatan);
    });

    $('.ktpimg').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/ktp/'+idnikah+'.jpg'
        $('#imgktp').attr("src", srcimg);
    });

    $('.aktekematian').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/aktekematian/'+idnikah+'.jpg'
        $('#imgaktekematian').attr("src", srcimg);
    });

    $('.akteperceraian').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/akteperceraian/'+idnikah+'.jpg'
        $('#imgakteperceraian').attr("src", srcimg);
    });

    $('.suratijintnipolri').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/suratijintnipolri/'+idnikah+'.jpg'
        $('#imgakteperceraian').attr("src", srcimg);
    });

    $('.foto3x4').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/foto3x4/'+idnikah+'.jpg'
        $('#imgfoto3x4').attr("src", srcimg);
    });

    $('.aktekelahiran').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/aktekelahiran/'+idnikah+'.jpg'
        $('#imgfoto3x4').attr("src", srcimg);
    });

    $('.kk').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/kk/'+idnikah+'.jpg'
        $('#imgkk').attr("src", srcimg);
    });


    $('.bukunikahimg').click(function(){
        var status = $(this).attr('data-statusorg');
        var idnikah = $(this).attr('data-idnikah');
        var nama = $(this).attr('data-nama');
        var srcimg = '/pernikahan/'+status+'/'+idnikah+'.jpg'
        $('#imgbukunikah').attr("src", srcimg);
    });
    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection