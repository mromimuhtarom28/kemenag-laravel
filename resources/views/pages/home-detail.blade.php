@extends('index')

@section('content')
<h1 class="mt-4">Dashboard vihara {{ $namavihara->nama_vihara }}</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard vihara {{ $namavihara->nama_vihara }}</li>
</ol>
{{-- <div class="row">
    <div class="col-xl-3 col-md-6">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body">Primary Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-warning text-white mb-4">
            <div class="card-body">Warning Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-success text-white mb-4">
            <div class="card-body">Success Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6">
        <div class="card bg-danger text-white mb-4">
            <div class="card-body">Danger Card</div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="#">View Details</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
</div> --}}
<div class="row">
    <div class="col-xl-6">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-chart-area mr-1"></i>
                Jumlah pernikahan perbulan selama satu tahun  di vihara {{ $namavihara->nama_vihara }} 
            </div>
            <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-chart-bar mr-1"></i>
                Jumlah pernikahan selama 5 Tahun terakhir  di vihara {{ $namavihara->nama_vihara }} 
            </div>
            <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Data pernikahan berdasarkan tahun Kelahiran Suami  di Vihara {{ $namavihara->nama_vihara }} 
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tahun</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tgllahirsuami as $suami)
                            <tr>
                                <td>{{ $suami->date_born_husband }}</td>
                                <td>{{ $suami->total_wed }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div style="display: flex;justify-content: center;">{{ $tgllahirsuami->links('pagination.default') }}</div>
                </div>
            </div>
        </div> 
    </div>
    <div class="col">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Data pernikahan berdasarkan tahun Kelahiran Istri  di vihara {{ $namavihara->nama_vihara }} 
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Tahun</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tgllahiristri as $istri)
                            <tr>
                                <td>{{ $istri->date_born_wife }}</td>
                                <td>{{ $istri->total_wed }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div style="display: flex;justify-content: center;">{{ $tgllahiristri->links('pagination.default') }}</div>
                </div>
            </div>
        </div>
    </div>
</div>   

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data pernikahan Perhari  di Vihara {{ $namavihara->nama_vihara }}
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($harianpernikahan as $harian)
                    <tr>
                        <td>{{ $harian->date_wed }}</td>
                        <td>{{ $harian->total_wed }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
             <div style="display: flex;justify-content: center;">{{ $harianpernikahan->links('pagination.default') }}</div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 

    // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [{{$labelyear }}],
    datasets: [{
      label: "Revenue",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: [@foreach($datacountyear as $key => $ttl){{$ttl}} @if($key != $totalyc-1){{','}} @endif @endforeach],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

// Area Chart Example
var ctx2 = document.getElementById("myAreaChart");
var myLineChart2 = new Chart(ctx2, {
  type: 'line',
  data: {
    labels: [@foreach($monthname as $key => $mthn) "{{ $mthn }}" @if($key != $totallabelmonth-1){{','}} @endif  @endforeach],
    datasets: [{
      label: "Sessions",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [@foreach($datacountmonth as $key => $dtmnt) "{{ $dtmnt }}" @if($key != $totalmc-1){{','}} @endif  @endforeach],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          maxTicksLimit: 5
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});






</script>
@endsection