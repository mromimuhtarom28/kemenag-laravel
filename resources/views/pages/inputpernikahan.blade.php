@extends('index')

@section('content')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active border border-dark" style="width:100%;">
        <form action="{{ route('Pernikahan-processInsert') }}" method="post" enctype="multipart/form-data" style="width:100%">
            @csrf
            <table align="center">
                <tr>
                    <td Colspan="3" style="color:black;font-weight:bold;" align="center" height="105px">Penginputan Perkawinan</td>
                </tr>
                <tr>
                    <td width="47%" style="color:black;font-weight:bold;">No Surat Keterangan Kawin</td>
                    <td width="6%" align="center">:</td>
                    <td width="47%"><input type="number" name="noKeteranganKawin" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Foto halaman Surat Keterangan Kawin</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="suratKeteranganKawin" required></td>
                </tr>
                @if($idrole == 1)
                <tr>
                    <td style="color:black;font-weight:bold;">Vihara</td>
                    <td align="center">:</td>
                    <td>
                        <select name="vihara" class="form-control vihara" required>
                            <option value="">Pilih Vihara</option>
                            @foreach ($vihara as $vh)
                            <option value="{{ $vh->id_vihara }}">Nama Vihara : {{ $vh->nama_vihara }} Alamat : {{ $vh->alamat }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Nama pandita yang menikahkan</td>
                    <td align="center">:</td>
                    <td>
                        <select class="form-control nama-pandita" name="namaPandita" required>
                            <option value="">Pilih Pandita</option>
                        </select>
                    </td>
                </tr>
                @else
                <tr>
                    <td style="color:black;font-weight:bold;">Nama pandita yang menikahkan</td>
                    <td align="center">:</td>
                    <td>
                        <select class="form-control" name="namaPandita" required>
                            <option value="">Pilih Pandita</option>
                            @foreach ($pandita as $pd)
                            <option value="{{ $pd->id_pandita }}">{{ $pd->nama_lengkap}}</option>                                
                            @endforeach
                        </select>
                    </td>
                </tr>
                @endif
                <tr>
                    <td style="color:black;font-weight:bold;">Tanggal Nikah</td>
                    <td align="center">:</td>
                    <td><input type="datetime-local" name="tglNikah" class="form-control" required></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="font-weight:bold">Riwayat Suami</td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Nama Suami</td>
                    <td align="center">:</td>
                    <td><input type="text" name="namaSuami" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">foto Suami 3x4</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoSuami3x4" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Surat Ijin dari komandan bagi Anggota TNI dan POLRI Suami (Boleh Kosong)</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="suratIjinTniPolriSuami"></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">NIK Suami</td>
                    <td align="center">:</td>
                    <td><input type="text" name="nikSuami" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Tanggal Lahir Suami</td>
                    <td align="center">:</td>
                    <td><input type="date" name="tglLhrSuami" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color: black;font-weight:bold;">Upload Akte Kelahiran Suami</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="akteKelahiranSuami" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto KTP Suami</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="ktpSuami" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto KK Suami </p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="kkSuami" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto Akta Perceraian Suami (Jika pernah menikah)(Boleh Kosong)</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoAktePerceraianSuami"></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto Akta Kematian (Pihak Suami jika Istri Meninggal) (Jika pernah menikah)(Boleh Kosong)</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoAkteKematianIstri"></td>
                </tr>
                <tr>
                    <td colspan="3" style="font-weight:bold" align="center">Riwayat Istri</td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Nama Istri</td>
                    <td align="center">:</td>
                    <td><input type="text" name="namaIstri" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">foto Istri 3x4</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoIstri3x4" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Surat Ijin dari komandan bagi Anggota TNI dan POLRI Istri (Boleh Kosong)</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="suratIjinTniPolriIstri"></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto Akta Perceraian Istri (Jika pernah menikah)</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoAktePerceraianIstri"></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto Akta Kematian (Pihak Istri jika Suami Meninggal) (Jika pernah menikah)</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="fotoAkteKematianSuami"></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">NIK Istri</td>
                    <td align="center">:</td>
                    <td><input type="text" name="nikIstri" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Tanggal Lahir Istri</td>
                    <td align="center">:</td>
                    <td><input type="date" name="tglLhrIstri" class="form-control" required></td>
                </tr>
                <tr>
                    <td style="color: black;font-weight:bold;">Upload Akte Kelahiran Istri</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="akteKelahiranIstri" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto KTP Istri</td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="ktpIstri" required></td>
                </tr>
                <tr>
                    <td style="color:black;font-weight:bold;">Input foto KK Istri</p></td>
                    <td align="center">:</td>
                    <td><input type="file" accept="image/jpeg" name="kkIstri" required></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" height="105px"><button type="submit" class="btn btn-primary">Simpan</button></td>
                </tr>
            </table>
        </form>
    </li>
</ol>

<script>
    $( ".vihara" ).change(function() {
        var id_vihara = $(this).val();
        $('.panditaopt').remove();
        $.ajax({
            url: '{{ route("list-pandita") }}',
            type: 'post',
            data:{
                idvihara:id_vihara,
                _token: "{{ csrf_token() }}"
            },
            success: function(response){
                //add response in modal body
                var obj = response;
                $.each( obj.pandita, function(index, pndt ) {
                    $('.nama-pandita').append('<option value="'+pndt.id_pandita+'" class="panditaopt">'+pndt.nama_lengkap+'</option>');                    
                });

            }
        });

    });
</script>
@endsection