@extends('index')

@section('content')
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imginsert').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }

    function readURLeditimg(input, idvihara) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgedit'+idvihara).attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Data Vihara
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <table width="100%">
                    <tr>
                        <td align="left"><a href="#" data-toggle="modal" data-target="#createadmin" class="btn btn-primary">Menambahkan Vihara</a></td>
                        <td align="right">
                            <form action="{{ route('Vihara') }}">
                            <table>
                                <tr>
                                    <td><input type="text" name="kodeVihara" class="form-control" placeholder="Kode Vihara" @if(isset($kode_vihara)) value="{{ $kode_vihara }}" @endif></td>
                                    <td><input type="text" name="namaVihara" class="form-control" placeholder="Nama Vihara" @if(isset($nama_vihara)) value="{{ $nama_vihara }}" @endif></td>
                                    <td><button type="submit" class="btn btn-primary">Cari</button></td>
                                </tr>
                            </table>
                            </form>
                            
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Gambar Vihara</th>
                        <th>Kode Vihara</th>
                        <th>Nama Vihara</th>
                        <th>Alamat</th>
                        <th>status</th>
                        {{-- <th></th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vihara as $vh)
                        <tr>
                            <td align="center" class="kolomeditgambar{{ $vh->id_vihara }}">
                                <div class="gambarbelumedit{{ $vh->id_vihara }}"  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                    <img class="gambarbelumedit{{ $vh->id_vihara }}" src="/upload/profile/vihara/{{ $vh->id_vihara }}.jpg" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                                </div>
                                <br class="gambarbelumedit{{ $vh->id_vihara}}">
                                <a href="#" class="btn btn-primary ubahgambar{{ $vh->id_vihara}}">Ubah Gambar</a> 
                            </td>
                            <td>{{ $vh->id_vihara }}</td>
                            <td><a href="#" class="textnormal" data-name="nama_vihara" data-pk="{{ $vh->id_vihara }}" data-type="text" data-value="{{ $vh->nama_vihara }}" data-url="{{ route('Vihara-update') }}">{{ $vh->nama_vihara }}</a></td>
                            <td><a href="#" class="datetest" data-name="alamat" data-pk="{{ $vh->id_vihara }}" data-type="date" data-value="{{ $vh->alamat }}" data-url="{{ route('Vihara-update') }}">{{ $vh->alamat }}</a></td>
                            <td><a href="#" class="statustext" data-name="status" data-pk="{{ $vh->id_vihara }}" data-type="select" data-value="{{ $vh->status }}" data-url="{{ route('Vihara-update') }}">{{ str_endis($vh->status) }}</a></td>
                            {{-- <td><i class="fas fa-times" data-toggle="modal" data-target="#deletevihara" style="color:red"></i></td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $vihara->links('pagination.default') }}</div>
        </div>
    </div>
</div> 


{{-- insert data --}}
<div class="modal fade" id="createadmin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Vihara</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Vihara-processInsert') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <div style="border-radius:10px;border:1px solid black;background-color:#cccccc;width:200px;height:100px;position: relative;display: inline-block;">
                                    <img src="/admin/assets/img/uploadimg.png" alt="" id="imginsert" max-width="100px" height="98px">
                                </div><br>                                    
                                <input type="file" accept="image/jpg" name="file" id="btnimginsert" onchange="readURL(this);" required>
                            </td>
                        </tr>
                    </table><br>
                    <input type="text" name="namaVihara" placeholder="Nama Vihara" class="form-control" required><br>  
                    <textarea name="alamat" class="form-control" cols="30" rows="10" placeholder="Alamat" required></textarea><br>
                    <select name="status" class="form-control" required>
                        <option value="">Pilih status</option>
                        <option value="0">Tidak Aktif</option>
                        <option value="1">Aktif</option>
                    </select>                  
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Delete Data --}}
<div class="modal fade" id="deletevihara" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menghapus Vihara</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Admin-delete') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    Apakah anda yakin ingin menghapus vihara ini ? <br>
                    <input type="hidden" name="pk" id="pk-destroy">
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Hapus Akun</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>






<script>

    $('.deleteaccount').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-destroy').val(pk);
    })

    @foreach($vihara as $vh)
        $(".ubahgambar{{ $vh->id_vihara }}").on("click", function() {
            $(this).hide();
            $(".gambarbelumedit{{ $vh->id_vihara }}").css("display", "none");
            $(".kolomeditgambar{{ $vh->id_vihara }}").append('<form method="POST" action="{{ route("Vihara-ImgChange") }}" enctype="multipart/form-data" class="editgambar{{ $vh->id_vihara }}"> @csrf <div style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;"><img src="/upload/profile/vihara/{{ $vh->id_vihara }}.jpg" id="imgedit{{ $vh->id_vihara }}" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;"></div><br><input type="hidden" name="pk" value="{{ $vh->id_vihara }}"><input type="file" onchange="readURLeditimg(this, {{ $vh->id_vihara }})" class="btn btn-secondary" accept="image/jpeg" name="file-vihara" id="editgambar" required><br><button type="submit" class="btn btn-success">Simpan</button> <a href="#" class="btn btn-danger editgambar" id="bataleditgambar{{ $vh->id_vihara}}">Batal</a></form>');
        });

        $('.kolomeditgambar{{ $vh->id_vihara}}').on('click', '#bataleditgambar{{ $vh->id_vihara }}', function(){
            $(".ubahgambar{{ $vh->id_vihara}}").show();
            $(".gambarbelumedit{{ $vh->id_vihara}}").css("display", "block");
            $(".editgambar{{ $vh->id_vihara}}").remove();
        });
    @endforeach
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('.textnormal').editable({
                        mode :'inline',
                        validate: function(value) {
                            if($.trim(value) == '') {
                            return 'Tidak boleh kosong';
                            }
                        }
                    });


                    $('.datetest').editable({
                        format: 'yyyy-mm-dd',  
                        mode : 'popup',
                        viewformat: 'dd/mm/yyyy',    
                        datepicker: {
                                weekStart: 1
                        }
                    });



                    $('.statustext').editable({
                        mode:'inline',
                        source: [
                            {value: '', text: 'Pilih Kelas'},
                            {value: 0, text: 'Tidak Aktif'},
                            {value: 1, text: 'Aktif'},
                        ],
                        validate: function(value) {
                        if($.trim(value) == '') {
                            return 'Tidak boleh kosong';
                        }
                        }
                    });
            }
        });
    }); 
</script>
@endsection