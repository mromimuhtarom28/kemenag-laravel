@extends('index')

@section('content')
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imginsert').attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }

    function readURLeditimg(input, idlogin) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#imgedit'+idlogin).attr('src', e.target.result);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
</script>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        Akun Admin Disdukcapil
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <table width="100%">
                    <tr>
                        <td align="left"><a href="#" data-toggle="modal" data-target="#createadmin" class="btn btn-primary">Membuat Akun Admin Disdukcapil</a></td>
                        <td align="right">
                            <form action="{{ route('Disdukcapil') }}">
                            <table>
                                <tr>
                                    <td><input type="text" name="username" class="form-control" placeholder="Nama Pengguna" @if(isset($username)) value="{{ $username }}" @endif></td>
                                    <td><button type="submit" class="btn btn-primary">Cari</button></td>
                                </tr>
                            </table>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Gambar Profil</th>
                        <th>Nama Pengguna</th>
                        <th>Nama Lengkap</th>
                        <th>Tanggal Lahir</th>
                        <th>Telp</th>
                        <th>Ganti Kata Sandi</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($admin as $adm)
                        <tr>
                            <td align="center" class="kolomeditgambar{{ $adm->id_login }}">
                                <div class="gambarbelumedit{{ $adm->id_login }}"  style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;">
                                    <img class="gambarbelumedit{{ $adm->id_login }}" src="/upload/profile/user/{{ $adm->id_login }}.jpg" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;magin-top:auto;margin-bottom:auto;">
                                </div>
                                <br class="gambarbelumedit{{ $adm->id_login}}">
                                <a href="#" class="btn btn-primary ubahgambar{{ $adm->id_login}}">Ubah Gambar</a> 
                            </td>
                            <td>{{ $adm->username }}</td>
                            <td>{{ $adm->nama_lengkap }}</td>
                            <td>{{ $adm->ttl }}</td>
                            <td>{{ $adm->telp }}</td>
                            <td><a href="#" data-pk="{{ $adm->id_login }}" data-toggle="modal" data-target="#updatepwd" class="btn btn-primary changepwd">Ganti Kata Sandi</a></td>
                            <td><a href="#" data-pk="{{ $adm->id_login }}" data-toggle="modal" data-target="#deleteakun" class="deleteaccount" style="color:red;font-weight:bold">&times;</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="display: flex;justify-content: center;">{{ $admin->links('pagination.default') }}</div>
        </div>
    </div>
</div> 


{{-- insert data --}}
<div class="modal fade" id="createadmin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Akun Disdukcapil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Disdukcapil-processInsert') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <div style="border-radius:10px;border:1px solid black;background-color:#cccccc;width:200px;height:100px;position: relative;display: inline-block;">
                                    <img src="/admin/assets/img/uploadimg.png" alt="" id="imginsert" max-width="100px" height="98px">
                                </div><br>                                    
                                <input type="file" accept="image/jpg" name="file" id="btnimginsert" onchange="readURL(this);" required>
                            </td>
                        </tr>
                    </table><br>
                    <div class="breadcrumb" align="left">
                        Catatan : <br>
                        - nama pengguna tidak boleh ada spasi dan tidak boleh melebihi 50 karakter
                    </div>
                    <span>Nama Pengguna</span><br>
                    <input type="text" name="namaPengguna" placeholder="Nama Pengguna" class="form-control" required pattern="^\S+$"><br>    
                    <span>Nama Lengkap</span><br>
                    <input type="text" name="namaLengkap" class="form-control" placeholder="Nama Lengkap" required><br>
                    <span>Email</span><br>
                    <input type="email" name="email" class="form-control" placeholder="email" required><br>
                    <span>Tanggal Lahir</span><br>
                    <input type="date" name="tanggalLahir" class="form-control" required><br>
                    <span>No. Telp/ HP</span><br>
                    <input type="number" name="noTelp" placeholder="No Telp" class="form-control" required><br>
                    <span>Kata Sandi</span><br>
                    <input type="password" name="kataSandi" placeholder="Kata Sandi" class="form-control" required><br>
                    <span>Konfirmasi Kata Sandi</span><br>
                    <input type="password" name="konfirmasiKataSandi" placeholder="Konfirmasi Kata Sandi" class="form-control" required><br>
                    <span>Alamat</span><br>
                    <textarea name="alamat" class="form-control" cols="30" rows="10" placeholder="alamat" required></textarea>               
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Delete Data --}}
<div class="modal fade" id="deleteakun" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Menambahkan Akun Admin Disdukcapil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Disdukcapil-delete') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    Apakah anda yakin ingin menghapus akun ini ? <br>
                    <input type="hidden" name="pk" id="pk-destroy">
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div> 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Hapus Akun</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Update password --}}
<div class="modal fade" id="updatepwd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Mengubah Kata sandi admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Disdukcapil-updatepwd') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body" align="center">
                    <div class="breadcrumb" align="left">
                        Catatan : <br>
                        - untuk kata sandi yang sedang login adalah dari akun yang lagi login sekarang
                    </div>
                    <input type="hidden" name="pk" id="pk-pwd">
                    <input type="password" name="kataSandiBaru" placeholder="Kata Sandi Baru" class="form-control" required><br>
                    <input type="password" name="kataSandiSedangLogin" placeholder="Kata sandi akun anda" class="form-control" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ubah Kata Sandi</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>





<script>
    $('.deleteaccount').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-destroy').val(pk);
    });

    $('.changepwd').click(function(){
        var pk = $(this).attr('data-pk');
        $('#pk-pwd').val(pk);
    });
    @foreach($admin as $adm)
        $(".ubahgambar{{ $adm->id_login }}").on("click", function() {
            console.log('asdf');
            $(this).hide();
            $(".gambarbelumedit{{ $adm->id_login }}").css("display", "none");
            $(".kolomeditgambar{{ $adm->id_login }}").append('<form method="POST" action="{{ route("Admin-ImgChange")}}" enctype="multipart/form-data" class="editgambar{{ $adm->id_login}}"> @csrf <div style="border-radius:5px;border:1px solid black;position: relative;display: inline-block;width:200px;height:100px;"><img src="/upload/profile/user/{{ $adm->id_login }}.jpg" id="imgedit{{ $adm->id_login }}" alt="" style="display: block;max-width:190px;max-height:90px;margin-left: auto; margin-right: auto;"></div><br><input type="hidden" name="pk" value="{{ $adm->id_login }}"><input type="file" onchange="readURLeditimg(this,{{ $adm->id_login }})" class="btn btn-secondary" accept="image/jpeg" name="file" id="editgambar" required><br><button type="submit" class="btn btn-success">Simpan</button> <a href="#" class="btn btn-danger editgambar" id="bataleditgambar{{ $adm->id_login}}">Batal</a></form>');
        });

        $('.kolomeditgambar{{ $adm->id_login}}').on('click', '#bataleditgambar{{ $adm->id_login }}', function(){
            $(".ubahgambar{{ $adm->id_login}}").show();
            $(".gambarbelumedit{{ $adm->id_login}}").css("display", "block");
            $(".editgambar{{ $adm->id_login}}").remove();
        });
    @endforeach
    $(document).ready(function() {
        $('table.table').DataTable({
            "bLengthChange": false,
            "searching": false,
            "paging":false,
            "bInfo":false,
            "ordering": false,
        });
    }); 
</script>
@endsection