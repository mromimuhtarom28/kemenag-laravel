<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->name('login-view');  
   
Route::group(['prefix' => 'login'], function() {
    Route::post('/login-process', 'LoginController@Login')->name('Login-Process');
    Route::get('/logout-process', 'LoginController@LogOut')->name('logout-process'); 
});

Route::middleware('authlogin')->group(function(){
    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Dashboard'], function(){
            Route::get('/Dashboard-view', 'HomeController@index')->name('Dashboard');
            Route::get('/Dahsboard-detail', 'HomeController@HomeDetail')->name('Dashboard-detail');
        });
    });

    Route::group(['prefix' => 'Profile'], function(){
        Route::get('/Profil-view', 'ProfilController@index')->name('Profile');
        Route::post('/Profile-update', 'ProfilController@UpdateProfile')->name('Profile-Update');
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Pernikahan'], function(){
            Route::get('/pernikahan-view', 'PernikahanController@index')->name('Pernikahan');
            Route::get('/pernikahan-inputpage', 'PernikahanController@inputpage')->name('Pernikahan-inputpage');
            Route::post('/pernikahan-proces-input', 'PernikahanController@insert')->name('Pernikahan-processInsert');
            Route::post('/list-pandita', 'PernikahanController@pandita')->name('list-pandita');
            Route::post('pernikahan-update', 'PernikahanController@update')->name('Pernikahan-update');
            Route::post('pernikahan-delete', 'PernikahanController@destroy')->name('Pernikahan-delete');
            Route::post('/pernikahan-ubah-aktepernikahan', 'PernikahanController@ubahakte')->name('Pernikahan-Ubahakte');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Admin'], function(){
            Route::get('/admin-view', 'AdminViharaController@index')->name('Admin');
            Route::post('/admin-approve', 'AdminViharaController@Approve')->name('Admin-Approve');
            Route::post('/admin-decline', 'AdminViharaController@Decline')->name('Admin-Decline');
            Route::post('/admin-ImgChange', 'AdminViharaController@changeimage')->name('Admin-ImgChange');
            Route::post('/admin-proces-input', 'AdminViharaController@insert')->name('Admin-processInsert');
            Route::post('/admin-updatepwd', 'AdminViharaController@changepwd')->name('Admin-updatepwd');
            Route::post('/admin-delete', 'AdminViharaController@destroydata')->name('Admin-delete');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Vihara'], function(){
            Route::get('vihara-view', 'ViharaController@index')->name('Vihara');
            Route::post('vihara-imgchange', 'ViharaController@updateImage')->name('Vihara-ImgChange');
            Route::post('vihara-process-input', 'ViharaController@insert')->name('Vihara-processInsert');
            Route::post('vihara-update', 'ViharaController@update')->name('Vihara-update');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Admin-Disdukcapil'], function(){
            Route::get('Admin-Disdukcapil-view', 'DisdukcapilController@index')->name('Disdukcapil');
            Route::post('Admin-Disdukcapil-imgchange', 'DisdukcapilController@updateImage')->name('Disdukcapil-ImgChange');
            Route::post('Admin-Disdukcapil-process-input', 'DisdukcapilController@insert')->name('Disdukcapil-processInsert');
            Route::post('Admin-Disdukcapil-delete', 'DisdukcapilController@destroydata')->name('Disdukcapil-delete');
            Route::post('/Admin-Disdukcapil-updatepwd', 'DisdukcapilController@changepwd')->name('Disdukcapil-updatepwd');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Request-AktePerkawinan'], function(){
            Route::get('Request-AktePerkawinan-view', 'RequestPerkawinanController@index')->name('Request-AktePerkawinan');
            Route::post('Request-AktePerkawinan-approve', 'RequestPerkawinanController@approve')->name('Request-AktePerkawinan-Approve');
            Route::post('Request-AktePerkawinan-decline', 'RequestPerkawinanController@decline')->name('Request-AktePerkawinan-Decline');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'Request-AktePerkawinan'], function(){
            Route::get('Request-AktePerkawinan-view', 'RequestPerkawinanController@index')->name('Request-AktePerkawinan');
            Route::post('Request-AktePerkawinan-approve', 'RequestPerkawinanController@approve')->name('Request-AktePerkawinan-Approve');
            Route::post('Request-AktePerkawinan-decline', 'RequestPerkawinanController@decline')->name('Request-AktePerkawinan-Decline');
        });
    });

    Route::middleware('ActivationAccount')->group(function(){
        Route::group(['prefix' => 'List-Ditolak-Disdukcapil'], function(){
            Route::get('List-Ditolak-Disdukcapil-view', 'ListDitolakDisdukcapilController@index')->name('List-Ditolak-Disdukcapil');
            Route::post('List-Ditolak-Disdukcapil-update', 'ListDitolakDisdukcapilController@update')->name('List-Ditolak-Disdukcapil-Update');
        });
    });
});
