<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileLogin extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'profil_login';
    protected $guarded = [];
    protected $primaryKey = 'id_profil';
}
