<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vihara extends Model
{
    use HasFactory;
    protected $table = 'vihara';
    protected $guarded = [];
    protected $primaryKey = 'id_vihara';
    public $timestamps = false;
    public $incrementing = false;
}
