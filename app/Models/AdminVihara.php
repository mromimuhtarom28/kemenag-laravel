<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminVihara extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'admin_vihara';
    protected $guarded = [];
    protected $primaryKey = 'id_adm_vihara';

    public function Vihara()
    {
        return $this->hasOne(Vihara::class, 'id_vihara', 'id_vihara');
    }
}
