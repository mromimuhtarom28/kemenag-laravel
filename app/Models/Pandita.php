<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pandita extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'pandita';
    protected $guarded = [];
    protected $primaryKey = 'id_pandita';

    public function ProfileLogin()
    {
        return $this->hasOne(ProfileLogin::class, 'id_profil', 'id_profil');
    }

    public function Vihara()
    {
        return $this->hasOne(Vihara::class, 'id_vihara', 'id_vihara');
    }
}
