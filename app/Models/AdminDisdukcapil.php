<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminDisdukcapil extends Model
{
    use HasFactory;
    protected $table = 'admin_disdukcapil';
    protected $guarded = [];
    protected $primaryKey = 'id_adm_disdukcapil';
    public $timestamps = false;
}
