<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pernikahan extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'pernikahan';
    protected $guarded = [];
    protected $primaryKey = 'id_pernikahan';

    public function Pandita()
    {
        return $this->hasOne(Pandita::class, 'id_pandita', 'id_pandita');
    }

    public function User() 
    {
        return $this->hasOne(User::class, 'id_login', 'id_login');
    }

    public function Vihara()
    {
        return $this->hasOne(Vihara::class, 'id_vihara', 'id_vihara');
    }
}
