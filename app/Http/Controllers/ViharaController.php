<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vihara;
use Validator;
use Session;
use Exception;

class ViharaController extends Controller
{
    public function index(Request $request)
    {
        $kode_vihara = $request->kodeVihara;
        $nama_vihara = $request->namaVihara;
        $id_role     = Session::get('id_role');

        if($kode_vihara != NULL && $nama_vihara != NULL): 
            $vihara = Vihara::select('vihara.id_vihara', 'nama_vihara', 'alamat', 'status')->where('id_vihara', $kode_vihara)
                      ->where('nama_vihara', 'LIKE', '%'.$nama_vihara.'%')
                      ->paginate(20);
        elseif($kode_vihara != NULL):
            $vihara = Vihara::select('vihara.id_vihara', 'nama_vihara', 'alamat', 'status')->where('id_vihara', $kode_vihara)
                      ->paginate(20);
        elseif($nama_vihara != NULL):
            $vihara = Vihara::select('vihara.id_vihara', 'nama_vihara', 'alamat', 'status')->where('nama_vihara', 'LIKE', '%'.$nama_vihara.'%')
                      ->paginate(20);
        else: 
            $vihara = Vihara::select('vihara.*')->paginate(20);
        endif;
        if($id_role == 1): 
        return view('pages.vihara', compact('kode_vihara', 'nama_vihara', 'vihara'));
        else: 
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back();
        endif;
    }

    public function updateImage(Request $request)
    {
        $pk = $request->pk;
        $img = $request->file('file-vihara');

        $validator = Validator::make($request->all(), [
            'pk'          => 'required|max:25',
            'file-vihara' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->error()->all());
            return back(); 
        endif;

        $img->move(public_path().'/upload/profile/vihara/', $pk.'.jpg');

        alert()->success('Ubah foto telah berhasil');
        return back();
    }

    public function insert(Request $request)
    {
        $img      = $request->file('file');
        $nmvihara = $request->namaVihara;
        $alamat   = $request->alamat;
        $status   = $request->status;

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpg',
            'namaVihara' => 'required|max:255',
            'alamat' => 'required',
            'status' => 'required|numeric'
        ]);

        if($validator->fails()):
            alert()->error($validator->error()->all());
            return back(); 
        endif;

        $arraystatus = array(0,1);
        if(!in_array($status, $arraystatus)):
            return 'status harus pilih aktif atau tidak aktif';
        endif;

        try {
            $vihara = Vihara::orderby('id_vihara', 'desc')->first();

            if($vihara):
                $idvihara =  idgenerate($vihara->id_vihara, 'VH');
            else:
                $idvihara = idgenerate('', 'VH');
            endif;
            $img->move(public_path().'/upload/profile/vihara/', $idvihara.'.jpg');

            Vihara::create([
                'id_vihara'   => $idvihara,
                'nama_vihara' => $nmvihara,
                'alamat'      => $alamat,
                'status'      => $status
            ]);

            alert()->success('Input data telah berhasil');
            return back();

        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }
    }

    public function update(Request $request)
    {
        $pk    = $request->pk;
        $name  = $request->name;
        $value = $request->value;

        Vihara::where('id_vihara', $pk)->update([
            $name => $value
        ]);
    }
}
