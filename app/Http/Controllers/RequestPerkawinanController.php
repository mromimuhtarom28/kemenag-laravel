<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pernikahan;
use Validator;
use Session;

class RequestPerkawinanController extends Controller
{
    public function index(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole != 4):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        endif;
        $nikah = Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita')
                      ->where('type', 1)
                      ->paginate(20);

        return view('pages.requestperkawinan', compact('nikah'));
    }

    public function approve(Request $request)
    {
        $pk             = $request->pk;
        $aktepernikahan = $request->aktePernikahan;
        $idrole = Session::get('id_role');
        if($idrole != 4):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        endif;

        $validator = Validator::make($request->all(), [
            'pk'             => 'required',
            'aktePernikahan' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;

        Pernikahan::where('id_nikah', $pk)->update([
            'type' => 2
        ]);

        $aktepernikahan->move(public_path()."/disdukcapil/", $pk.'.jpg');

        alert()->success('Data Berhasil diterima');
        return back();

    }

    public function decline(Request $request)
    {
        $pk      = $request->pk;
        $catatan = $request->catatan;
        $idrole = Session::get('id_role');
        if($idrole != 4):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        endif;

        $validator = Validator::make($request->all(), [
            'pk'      => 'required',
            'catatan' => 'required'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;

        Pernikahan::where('id_nikah', $pk)->update([
            'type'    => 0,
            'catatan' => $catatan
        ]);


        alert()->success('Data Berhasil ditolak');
        return back();
    }
}
