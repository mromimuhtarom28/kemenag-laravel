<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session; 
use App\Models\Pernikahan;
use Validator;
use App\Models\AdminVihara;

class ListDitolakDisdukcapilController extends Controller
{
    public function index(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole == 1):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        elseif($idrole == 4): 
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back();
        endif;
        $nikah = Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita')
                      ->where('type', 0)
                      ->paginate(20);

        return view('pages.listditolakdisdukcapil', compact('nikah'));
    }

    public function update(Request $request)
    {
        $idrole = Session::get('id_role');
        if($idrole == 1):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        elseif($idrole == 4): 
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back();
        endif;
        $bukunikah              = $request->file('suratKeteranganKawin');
        $nonikah                = $request->noKeteranganKawin;
        $niksuami               = $request->nikSuami;
        $namasuami              = $request->namaSuami;
        $tgllhrsuami            = $request->tanggalLahirSuami;
        $ktpsuami               = $request->file('ktpSuami');
        $kksuami                = $request->file('kkSuami');
        $fotosuami              = $request->file('fotoSuami3x4');
        $aktekelahiransuami     = $request->file('fotoAkteKelahiranSuami');
        $akteperceraiansuami    = $request->file('fotoAktePerceraianSuami');
        $aktekematianistri      = $request->file('fotoAkteKematianIstri');
        $suratijinpolritnisuami = $request->file('suratIjinTniPolriSuami');
        $nikistri               = $request->nikIstri;
        $namaistri              = $request->namaIstri;
        $tgllhristri            = $request->tanggalLahirIstri;
        $namapandita            = $request->namaPandita;
        $tglnikah               = $request->tanggalNikah;
        $jamNikah               = $request->jamNikah;
        $ktpistri               = $request->file('ktpIstri');
        $kkistri                = $request->file('kkIstri');
        $fotoistri              = $request->file('fotoIstri3x4');
        $aktekelahiranistri     = $request->file('fotoAkteKelahiranIstri');
        $akteperceraianistri    = $request->file('fotoAktePerceraianIstri');
        $aktekematiansuami      = $request->file('fotoAkteKematianSuami');
        $suratijinpolritniistri = $request->file('suratIjinTniPolriIstri');
        $idnikah                = $request->id_nikah;
        $idlogin                = Session::get('idlogin');
        $idrole                 = Session::get('id_role');

        
        if($bukunikah):
            $img = Validator::make($request->all(), [
                'suratKeteranganKawin' => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        // --- Suami ---- //
        if($ktpsuami):
            $img = Validator::make($request->all(), [
                'ktpSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        if($kksuami): 
            $img = Validator::make($request->all(), [
                'kkSuami'  => 'required|mimes:jpg'
            ]);  
           
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
        if($fotosuami):
            $img = Validator::make($request->all(), [
                'fotoSuami3x4'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekelahiransuami): 
            $img = Validator::make($request->all(), [
                'fotoAkteKelahiranSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($akteperceraiansuami): 
            $img = Validator::make($request->all(), [
                'fotoAktePerceraianSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekematianistri): 
            $img = Validator::make($request->all(), [
                'fotoAkteKematianIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($suratijinpolritnisuami): 
            $img = Validator::make($request->all(), [
                'suratIjinTniPolriSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        // ---- Istri ---- //
        if($ktpistri):
            $img = Validator::make($request->all(), [
                'ktpIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($kkistri): 
            $img = Validator::make($request->all(), [
                'kkIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($fotoistri):
            $img = Validator::make($request->all(), [
                'fotoIstri3x4'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekelahiranistri): 
            $img = Validator::make($request->all(), [
                'fotoAkteKelahiranIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($akteperceraianistri): 
            $img = Validator::make($request->all(), [
                'fotoAktePerceraianIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekematiansuami): 
            $img = Validator::make($request->all(), [
                'fotoAkteKematianSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
        if($suratijinpolritniistri): 
            $img = Validator::make($request->all(), [
                'suratIjinTniPolriIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
            $userlogin   = AdminVihara::where('id_login', $idlogin)
                        ->first();
            
            
            $validator = Validator::make($request->all(), [
                'id_nikah'          => 'required',
                'noKeteranganKawin' => 'required',
                'namaPandita'       => 'required|max:5',
                'namaSuami'         => 'required|max:255',
                'nikSuami'          => 'required|max:20',
                'tanggalLahirSuami' => 'required',
                'namaIstri'         => 'required|max:255',
                'nikIstri'          => 'required|max:20',
                'tanggalLahirIstri' => 'required',
                'tanggalNikah'      => 'required',
                'jamNikah'          => 'required'
            ]);
            
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;

            try {   
                        
                $pernikahan = Pernikahan::where('id_nikah', $idnikah)->update([
                    'no_nikah'        => $nonikah,
                    'id_vihara'       => $userlogin->id_vihara,
                    'id_pandita'      => $namapandita,
                    'id_login'        => $idlogin,
                    'nama_suami'      => $namasuami,
                    'nik_suami'       => $niksuami,
                    'tgl_lahir_suami' => $tgllhrsuami,
                    'nama_istri'      => $namasuami,
                    'nik_istri'       => $nikistri,
                    'tgl_lahir_istri' => $tgllhristri,
                    'tgl_nikah'       => $tglnikah.' '.$jamNikah,
                    'type'            => 1,
                    'catatan'         => ''
                ]);
                

                if($bukunikah):
                    $bukunikah->move(public_path().'/pernikahan/bukupernikahan/', $idnikah.'.jpg');
                endif;
                // --- Suami ---- //
                if($ktpsuami):
                    $ktpsuami->move(public_path().'/pernikahan/suami/ktp/', $idnikah.'.jpg');
                endif;
    
                if($kksuami): 
                    $kksuami->move(public_path().'/pernikahan/suami/kk/', $idnikah.'.jpg');
                endif;
    
                if($fotosuami):
                    $fotosuami->move(public_path().'/pernikahan/suami/foto3x4/', $idnikah.'.jpg'); 
                endif;
    
                if($aktekelahiransuami): 
                    $aktekelahiransuami->move(public_path().'/pernikahan/suami/aktekelahiran/', $idnikah.'.jpg');
                endif;
    
                if($akteperceraiansuami): 
                    $akteperceraiansuami->move(public_path().'/pernikahan/suami/akteperceraian/', $idnikah.'.jpg');
                endif;
    
                if($aktekematianistri): 
                    $aktekematianistri->move(public_path().'/pernikahan/suami/aktekematian/', $idnikah.'.jpg');
                endif;
    
                if($suratijinpolritnisuami): 
                    $suratijinpolritnisuami->move(public_path().'/pernikahan/suami/suratijintnipolri/', $idnikah.'.jpg');
                endif;
    
                // ---- Istri ---- //
                if($ktpistri):
                    $ktpistri->move(public_path().'/pernikahan/istri/ktp/', $idnikah.'.jpg');
                endif;
    
                if($kkistri): 
                    $kkistri->move(public_path().'/pernikahan/istri/kk/', $idnikah.'.jpg');
                endif;
    
                if($fotoistri):
                    $fotoistri->move(public_path().'/pernikahan/istri/foto3x4/', $idnikah.'.jpg'); 
                endif;
    
                if($aktekelahiranistri): 
                    $aktekelahiranistri->move(public_path().'/pernikahan/istri/aktekelahiran/', $idnikah.'.jpg');
                endif;
    
                if($akteperceraianistri): 
                    $akteperceraianistri->move(public_path().'/pernikahan/istri/akteperceraian/', $idnikah.'.jpg');
                endif;
    
                if($aktekematiansuami): 
                    $aktekematiansuami->move(public_path().'/pernikahan/istri/aktekematian/', $idnikah.'.jpg');
                endif;
    
                if($suratijinpolritniistri): 
                    $suratijinpolritniistri->move(public_path().'/pernikahan/istri/suratijintnipolri/', $idnikah.'.jpg');
                endif;

            } catch(Exception $e) {
                alert()->error($e->getMessage());
                return back();
            }
        


       

        alert()->success('Edit data telah berhasil');
        return back();
    }
}
