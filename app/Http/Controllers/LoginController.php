<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\AdminVihara;
use Validator;
use DB;
use Cache;
use App\Models\User;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function Login(Request $request)
    {
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])){

            if(Auth::user()->id_role == 3):
                $AdminVihara = AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                                ->join('login', 'admin_vihara.id_login', 'login.id_login')
                                ->where('admin_vihara.id_login', Auth::user()->id_login)
                                ->first();
                if($AdminVihara->status == 3):
                    alert()->error('Mohon maaf nama pengguna atau kata sandi anda salah silahkan coba lagi !!!');
                    return back();
                endif;
            endif;
            Session::put('idlogin', Auth::user()->id_login);
            Session::put('username', Auth::user()->username);
            Session::put('id_role',Auth::user()->id_role);
            Session::put('login',TRUE);
            $username        = $request->username;
            $password        = $request->password;

            if(Auth::user()->id_role == 3):
                $AdminVihara = AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                                ->join('login', 'admin_vihara.id_login', 'login.id_login')
                                ->where('admin_vihara.id_login', Auth::user()->id_login)
                                ->first();
                if($AdminVihara->status == 0): 
                    return redirect(route('Profile'));
                elseif($AdminVihara->status == 1):
                    alert()->error('Menunggu diterima dari admin kementrian agama');
                    return back();
                endif;
            endif;

            return redirect(route('Dashboard'));
  
         } else {
            $username = $request->username;
            alert()->error('ErrorAlert', 'Mohon maaf nama pengguna atau kata sandi anda salah silahkan coba lagi !!!');
            return back();

        }
    }

    public function LogOut()
    {
        Session::flush();
        Cache::flush();

        alert()->success('Terima kasih');
        return redirect()->route('login-view');
    }
}
