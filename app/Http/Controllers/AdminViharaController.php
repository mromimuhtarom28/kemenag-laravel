<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminVihara;
use App\Models\Pandita;
use Session;
use Validator;
use Exception;
use App\Models\User;
use App\Models\ProfileLogin;

class AdminViharaController extends Controller
{
    public function index(Request $request)
    {
        $username = $request->username;
        $id_role = Session::get('id_role');
        $idlogin = Session::get('idlogin'); 
        if($id_role == 1):
            if($username != NULL):
                if(is_numeric($username)):
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('nik', $username)
                            ->where('status', '!=', 3)
                            ->paginate(20);
                else: 
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('username', 'LIKE', '%'.$username.'%')
                            ->where('status', '!=', 3)
                            ->paginate(20);
                endif;
            else: 
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('status', '!=', 3)
                            ->paginate(20);
            endif;
        else:
            if($id_role == 3): 
                $user = AdminVihara::where('id_login', $idlogin)->first();
            elseif($id_role == 2): 
                $login = User::where('id_login', $idlogin)->first();
                $user = Pandita::where('id_profil', $login->id_profil)->first();
            endif;

            if($username != NULL):
                if(is_numeric($username)):
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('id_vihara', $user->id_vihara)
                            ->where('nik', $username)
                            ->where('status', '!=', 3)
                            ->paginate(20);
                else: 
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('id_vihara', $user->id_vihara)
                            ->where('username', 'LIKE', '%'.$username.'%')
                            ->where('status', '!=', 3)
                            ->paginate(20);
                endif;
            else: 
                    $admin = AdminVihara::Join('login', 'login.id_login', '=', 'admin_vihara.id_login')
                            ->join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->where('id_vihara', $user->id_vihara)
                            ->where('status', '!=', 3)
                            ->paginate(20);
            endif;
        endif;
        return view('pages.adminvihara', compact('username', 'admin', 'id_role'));
    }

    public function insert(Request $request)
    {
        $profile   = $request->file('file');
        $nik       = $request->nik;
        $fullname  = $request->namaLengkap;
        $username  = $request->namaPengguna;
        $email     = $request->email;
        $notelp    = $request->noTelp;
        $borndate  = $request->tanggalLahir;
        // $bornplace = $request->tempatLahir;
        $address   = $request->alamat;
        $idrole    = Session::get('id_role');
        if($idrole == 2 || $idrole == 3):
            $validator = Validator::make($request->all(), [
                'file'         => 'required|mimes:jpg',
                'nik'          => 'required|max:12',
                'namaLengkap'  => 'required|max:255',
                'namaPengguna' => 'required|max:50|unique:login,username',
                'email'        => 'required|max:255',
                'noTelp'       => 'required|max:15',
                'tanggalLahir' => 'required|date|date_format:Y-m-d',
                // 'tempatLahir'  => 'required',
                'alamat'       => 'required'
            ]);
        elseif($idrole == 1):
            $vihara = $request->vihara; 
            $validator = Validator::make($request->all(), [
                'file'         => 'required|mimes:jpg',
                'nik'          => 'required|max:12',
                'namaLengkap'  => 'required|max:255',
                'namaPengguna' => 'required|max:50|unique:login,username',
                'email'        => 'required|max:255',
                'noTelp'       => 'required|max:15',
                'tanggalLahir' => 'required|date|date_format:Y-m-d',
                // 'tempatLahir'  => 'required',
                'alamat'       => 'required',
                'vihara'       => 'required'
            ]);
        endif;

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back();
        endif;

        try {
            $bcrypt = bcrypt($nik);
            $profilelogin = ProfileLogin::create([
                'nama_lengkap' => $fullname,
                'alamat' => $address,
                'ttl'    => $borndate,
                'telp'   => $notelp
            ]);
            $login = User::create([
                'username'  => $username,
                'password'  => $bcrypt,
                'id_role'   => 3,
                'id_profil' => $profilelogin->id_profil
            ]);

            $profile->move(public_path().'/upload/profile/user/', $login->id.'.jpg');
            
            if($idrole == 2 || $idrole == 3): 
                $userid = Session::get('idlogin');
                $admin  = AdminVihara::where('id_login', $userid)->first();
                AdminVihara::create([
                    'id_login'  => $login->id_login,
                    'nik'       => $nik,
                    'email'     => $email,
                    'id_vihara' => $admin->id_vihara,
                    'id_profil' => $profilelogin->id_profil,
                    'status'    => 1,
                    'catatan'   => 'Menunggu proses penerimaan dari admin KEMENAG'
                ]);
            elseif($idrole == 1):
                AdminVihara::create([
                    'id_login'  => $login->id_login,
                    'nik'       => $nik,
                    'email'     => $email,
                    'id_vihara' => $vihara,
                    'id_profil' => $profilelogin->id_profil,
                    'status'    => 1,
                    'catatan'   => 'Menunggu proses penerimaan dari admin KEMENAG'
                ]);
            endif;
            
            alert()->success('Menambahkan akun admin telah berhasil');
            return back();
        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }
    }

    public function changeimage(Request $request)
    {
        $pk  = $request->pk;
        $img = $request->file('file');

        $validator = Validator::make($request->all(), [
            'pk' => 'required',
            'file' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $img->move(public_path().'/upload/profile/user/', $pk.'.jpg');

        alert()->success('Gambar profil berhasil di ganti');
        return back();
    }

    public function Approve(Request $request)
    {
        $pk = $request->pk;
        $validator = Validator::make($request->all(), [
            'pk' => 'required'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;
        AdminVihara::where('id_adm_vihara', $pk)->update([
            'status'  => 2,
            'catatan' => 'Akun sudah diterima'
        ]);

        alert()->success('Akun ini berhasil diterima');
        return back();

    }

    public function Decline(Request $request)
    {
        $pk = $request->pk;
        $catatan = $request->catatan;
        $validator = Validator::make($request->all(), [
            'pk'      => 'required',
            'catatan' => 'required'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;
        AdminVihara::where('id_adm_vihara', $pk)->update([
            'status'  => 0,
            'catatan' => $catatan
        ]);

        alert()->success('Akun ini berhasil diterima');
        return back();

    }

    public function changepwd(Request $request)
    {
        $pk       = $request->pk;
        $newpwd   = $request->kataSandiBaru;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $user     = User::where('id_login', $idlogin)->first();
        $validator = Validator::make($request->all(), [
            'kataSandiBaru'        => 'required|max:255',
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        if(password_verify($pwdlogin, $user->password)):
            $bcrypt = bcrypt($newpwd);
            User::where('id_login', $idlogin)->update([
                'password' => $bcrypt
            ]);

            alert()->success('Ubah kata sandi telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kemabli');
            return back();
        endif;
    }

    public function destroydata(Request $request)
    {
        $pk       = $request->pk;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $validator = Validator::make($request->all(), [
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $admin = AdminVihara::where('id_login', $pk)->first();
        $user  = User::where('id_login', $idlogin)->first();
        if(password_verify($pwdlogin, $user->password)):
            AdminVihara::where('id_login', $pk)->update([
                'status' => 3
            ]);

            alert()->success('Hapus akun telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kemabli');
            return back();
        endif;

    }

}
