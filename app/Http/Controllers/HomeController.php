<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\AdminVihara;
use App\Models\Pernikahan;
use App\Models\Pandita;
use App\Models\Vihara;
use App\Models\User;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $id_role = Session::get('id_role');
        $idlogin = Session::get('idlogin');
        if($id_role == 3 || $id_role == 2): 
            if($id_role == 3): 
            $user = AdminVihara::where('id_login', $idlogin)->first();
            elseif($id_role == 2): 
                $login = User::where('id_login', $idlogin)->first();
                $user = Pandita::where('id_profil', $login->id_profil)->first();
            endif;
            $harianpernikahan = Pernikahan::select(DB::raw('DATE(tgl_nikah) as date_wed'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $user->id_vihara)
                                ->groupBy(DB::raw('DATE(tgl_nikah)'))
                                ->paginate(5);
            $tgllahirsuami = Pernikahan::select(DB::raw('YEAR(tgl_lahir_suami) as date_born_husband'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $user->id_vihara)
                                ->groupBy(DB::raw('YEAR(tgl_lahir_suami)'))
                                ->paginate(5, ['*'], 'page_1s');
            $tgllahiristri = Pernikahan::select(DB::raw('YEAR(tgl_lahir_istri) as date_born_wife'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $user->id_vihara)
                                ->groupBy(DB::raw('YEAR(tgl_lahir_istri)'))
                                ->paginate(5, ['*'], 'page_2s');
            $countfiveyears = Pernikahan::select(DB::raw('YEAR(tgl_nikah) as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('id_vihara', $user->id_vihara)
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 5 YEAR )'))
                              ->groupBy(DB::raw('YEAR(tgl_nikah)'))
                              ->get();
            $yearname = array();
            $datacountyear = array();
            foreach($countfiveyears as $cfy): 
                $yearname[] = $cfy->year_wed;
                $datacountyear[] = $cfy->totalyearcount;
            endforeach;
            $labelyear = implode(',',$yearname);
            $totalyc = count($datacountyear);
            

            $countoneyear = Pernikahan::select(DB::raw('DATE_FORMAT(tgl_nikah, "%b") as month_wed'), DB::raw('DATE_FORMAT(tgl_nikah, "%y")  as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('id_vihara', $user->id_vihara)
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 1 YEAR )'))
                              ->groupBy(DB::raw("DATE_FORMAT(tgl_nikah,'%Y-%m')"))
                              ->get();

            $monthname = array();
            $datacountmonth = array();
            foreach($countoneyear as $coy): 
                $monthname[] = $coy->month_wed." ".$coy->year_wed;
                $datacountmonth[] = $coy->totalyearcount;
            endforeach;
            $totallabelmonth = count($monthname);
            $totalmc = count($datacountmonth);
            
            
            return view('pages.home', compact('user', 'harianpernikahan', 'tgllahiristri', 'tgllahirsuami', 'countfiveyears', 'labelyear', 'totalyc', 'datacountyear', 'monthname', 'totalmc' ,'totallabelmonth', 'datacountmonth', 'id_role'));
        elseif($id_role == 1 || $id_role == 4): 
            $harianpernikahan = Pernikahan::select(DB::raw('DATE(tgl_nikah) as date_wed'), DB::raw('COUNT(*) as total_wed'))
                                ->groupBy(DB::raw('DATE(tgl_nikah)'))
                                ->paginate(5);
            $tgllahirsuami = Pernikahan::select(DB::raw('YEAR(tgl_lahir_suami) as date_born_husband'), DB::raw('COUNT(*) as total_wed'))
                                ->groupBy(DB::raw('YEAR(tgl_lahir_suami)'))
                                ->paginate(5, ['*'], 'page_1s');
            $tgllahiristri = Pernikahan::select(DB::raw('YEAR(tgl_lahir_istri) as date_born_wife'), DB::raw('COUNT(*) as total_wed'))
                                ->groupBy(DB::raw('YEAR(tgl_lahir_istri)'))
                                ->paginate(5, ['*'], 'page_2s');
            $vihara = Pernikahan::Join('vihara', 'vihara.id_vihara', '=', 'pernikahan.id_vihara')
                                ->select('vihara.id_vihara', 'vihara.nama_vihara', DB::raw('COUNT(*) as total_wed'))
                                ->groupBy('pernikahan.id_vihara')
                                ->paginate(5, ['*'], 'page_3s');
            $countfiveyears = Pernikahan::select(DB::raw('YEAR(tgl_nikah) as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 5 YEAR )'))
                              ->groupBy(DB::raw('YEAR(tgl_nikah)'))
                              ->get();
            $yearname = array();
            $datacountyear = array();
            foreach($countfiveyears as $cfy): 
                $yearname[] = $cfy->year_wed;
                $datacountyear[] = $cfy->totalyearcount;
            endforeach;
            $labelyear = implode(',',$yearname);
            $totalyc = count($datacountyear);
            

            $countoneyear = Pernikahan::select(DB::raw('DATE_FORMAT(tgl_nikah, "%b") as month_wed'), DB::raw('DATE_FORMAT(tgl_nikah, "%y")  as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 1 YEAR )'))
                              ->groupBy(DB::raw("DATE_FORMAT(tgl_nikah,'%Y-%m')"))
                              ->get();

            $monthname = array();
            $datacountmonth = array();
            foreach($countoneyear as $coy): 
                $monthname[] = $coy->month_wed." ".$coy->year_wed;
                $datacountmonth[] = $coy->totalyearcount;
            endforeach;
            $totallabelmonth = count($monthname);
            $totalmc = count($datacountmonth);
            
            
            return view('pages.home', compact('harianpernikahan', 'tgllahiristri', 'tgllahirsuami', 'countfiveyears', 'labelyear', 'totalyc', 'datacountyear', 'monthname', 'totalmc' ,'totallabelmonth', 'datacountmonth', 'id_role', 'vihara'));
        else: 
            return back();
        endif;
    }

    public function HomeDetail(Request $request)
    {
            $id_role = Session::get('id_role');

            if($id_role != 1):
                alert()->error('Mohon maaf anda tidak bisa akses ke halaman ini');
                return back();
            endif;
            $vihara = $request->id_vihara;
            $harianpernikahan = Pernikahan::select(DB::raw('DATE(tgl_nikah) as date_wed'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $vihara)
                                ->groupBy(DB::raw('DATE(tgl_nikah)'))
                                ->paginate(5);
            $tgllahirsuami = Pernikahan::select(DB::raw('YEAR(tgl_lahir_suami) as date_born_husband'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $vihara)
                                ->groupBy(DB::raw('YEAR(tgl_lahir_suami)'))
                                ->paginate(5, ['*'], 'page_1s');
            $tgllahiristri = Pernikahan::select(DB::raw('YEAR(tgl_lahir_istri) as date_born_wife'), DB::raw('COUNT(*) as total_wed'))
                                ->where('id_vihara', $vihara)
                                ->groupBy(DB::raw('YEAR(tgl_lahir_istri)'))
                                ->paginate(5, ['*'], 'page_2s');
            $countfiveyears = Pernikahan::select(DB::raw('YEAR(tgl_nikah) as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('id_vihara', $vihara)
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 5 YEAR )'))
                              ->groupBy(DB::raw('YEAR(tgl_nikah)'))
                              ->get();
            $yearname = array();
            $datacountyear = array();
            foreach($countfiveyears as $cfy): 
                $yearname[] = $cfy->year_wed;
                $datacountyear[] = $cfy->totalyearcount;
            endforeach;
            $labelyear = implode(',',$yearname);
            $totalyc = count($datacountyear);
            

            $countoneyear = Pernikahan::select(DB::raw('DATE_FORMAT(tgl_nikah, "%b") as month_wed'), DB::raw('DATE_FORMAT(tgl_nikah, "%y")  as year_wed'), DB::raw('COUNT(*) as totalyearcount'))
                              ->where('id_vihara', $vihara)
                              ->where('tgl_nikah', '>', DB::raw('DATE_SUB(CURDATE(), INTERVAL 1 YEAR )'))
                              ->groupBy(DB::raw("DATE_FORMAT(tgl_nikah,'%Y-%m')"))
                              ->get();

            $monthname = array();
            $datacountmonth = array();
            foreach($countoneyear as $coy): 
                $monthname[] = $coy->month_wed." ".$coy->year_wed;
                $datacountmonth[] = $coy->totalyearcount;
            endforeach;
            $totallabelmonth = count($monthname);
            $totalmc = count($datacountmonth);
            $namavihara = Vihara::where('id_vihara', $vihara)->first();

            
            
            return view('pages.home-detail', compact('harianpernikahan', 'tgllahiristri', 'tgllahirsuami', 'countfiveyears', 'labelyear', 'totalyc', 'datacountyear', 'monthname', 'totalmc' ,'totallabelmonth', 'datacountmonth', 'id_role', 'namavihara'));
    }
}
