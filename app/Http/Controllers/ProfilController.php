<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdminVihara;
use App\Models\ProfileLogin;
use App\Models\User;
use Validator;
use Session;
use Exception;

class ProfilController extends Controller
{
    public function index() 
    {
        $idlogin = Session::get('idlogin');
        $idrole  = Session::get('id_role');

        if($idrole == 1 || $idrole == 2):
            alert()->error('Mohon maaf anda tidak ada hak untuk akses disini');
            return back();
        endif;
        if($idrole == 3):
        $AdminVihara = AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                        ->join('login', 'admin_vihara.id_login', 'login.id_login')
                        ->where('admin_vihara.id_login', $idlogin)
                        ->first();
        elseif($idrole == 4): 
            $AdminVihara = User::join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                            ->join('admin_disdukcapil', 'admin_disdukcapil.id_login', 'login.id_login')
                            ->first();
        endif;
        return view('pages.adminbiodata', compact('AdminVihara', 'idrole'));
    }

    public function UpdateProfile(Request $request)
    {
        $file     = $request->file('gambarProfile');
        $nik      = $request->nik;
        $fullname = $request->namaLengkap;
        $telp     = $request->noTelp;
        $tgllhr   = $request->tanggalLahir;
        $alamat   = $request->alamat;
        $pk       = $request->pk;
        $idrole   = Session::get('id_role');
        if($idrole == 1 || $idrole == 2):
            alert()->error('Mohon maaf anda tidak ada hak untuk akses disini');
            return back(); 
        endif;

        if($idrole == 3):
            $validator = Validator::make($request->all(), [
                'nik'          => 'required|max:12',
                'namaLengkap'  => 'required|max:255',
                'noTelp'       => 'required|max:15',
                'tanggalLahir' => 'required|date',
                'alamat'       => 'required',
                'pk'           => 'required'
            ]);
        elseif($idrole == 4): 
            $validator = Validator::make($request->all(), [
                'namaLengkap'  => 'required|max:255',
                'noTelp'       => 'required|max:15',
                'tanggalLahir' => 'required|date',
                'alamat'       => 'required',
                'pk'           => 'required'
            ]);
        endif;

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;
        if($file): 
            $validatorimg = Validator::make($request->all(), [
                'gambarProfile'          => 'mimes:jpg',
            ]);
    
            if($validatorimg->fails()):
                alert()->error($validatorimg->errors()->all());
                return back(); 
            endif;
        endif;

        if($idrole == 3):
            $AdminVihara = AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->join('login', 'admin_vihara.id_login', 'login.id_login')
                            ->where('admin_vihara.id_adm_vihara', $pk)
                            ->first();
        elseif($idrole == 4): 
            $AdminVihara = User::join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                            ->join('admin_disdukcapil', 'admin_disdukcapil.id_login', 'login.id_login')
                            ->where('login.id_login', $pk)
                            ->first();
        endif;

        try{

            if($idrole == 3):
                if($AdminVihara->status == 0):
                    AdminVihara::where('id_login', $AdminVihara->id_login)->update([
                        'nik'     => $nik,
                        'status'  => 1,
                        'catatan' => 'Menunggu proses penerimaan dari admin KEMENAG'
                    ]);
                else:
                    AdminVihara::where('id_login', $AdminVihara->id_login)->update([
                        'nik'     => $nik
                    ]); 
                endif;
            endif;
    
            ProfileLogin::where('id_profil', $AdminVihara->id_profil)->update([
                'nama_lengkap' => $fullname,
                'alamat'       => $alamat,
                'ttl'          => $tgllhr,
                'telp'         => $telp
            ]);
            if($file):
                $file->move(public_path().'/upload/profile/user/', $AdminVihara->id_login.'.jpg');
            endif;

            if($AdminVihara->status == 0): 
                return redirect()->route('logout-process');
            else: 
                alert()->success('Ubah data berhasil');
                return back();
            endif;

        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }
    }
}
