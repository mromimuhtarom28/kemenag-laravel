<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use App\Models\AdminDisdukcapil;
use Validator;
use App\Models\ProfileLogin;

class DisdukcapilController extends Controller
{
    public function index(Request $request)
    {
        $username = $request->username;
        $id_role = Session::get('id_role');
        $idlogin = Session::get('idlogin'); 
        if($id_role == 3):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        elseif($id_role == 2): 
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back(); 
        endif;

            if($username != NULL):
                    $admin = User::join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                            ->join('admin_disdukcapil', 'admin_disdukcapil.id_login', '=', 'login.id_login')
                            ->where('username', 'LIKE', '%'.$username.'%')
                            ->where('status', '!=', 3)
                            ->paginate(20);
            else: 
                $admin = User::join('profil_login', 'profil_login.id_profil', '=', 'login.id_profil')
                        ->join('admin_disdukcapil', 'admin_disdukcapil.id_login', '=', 'login.id_login')
                        ->where('status', '!=', 3)
                        ->paginate(20);
            endif;
     
        return view('pages.disdukcapil', compact('username', 'admin'));
    }

    public function insert(Request $request)
    {
        $profile   = $request->file('file');
        $fullname  = $request->namaLengkap;
        $username  = $request->namaPengguna;
        $notelp    = $request->noTelp;
        $borndate  = $request->tanggalLahir;
        $email     = $request->email;
        $address   = $request->alamat;
        $pwd       = $request->kataSandi;
        $cpwd      = $request->konfirmasiKataSandi;
        $idrole    = Session::get('id_role');

        $validator = Validator::make($request->all(), [
            'file'         => 'required|mimes:jpg',
            'namaLengkap'  => 'required|max:255',
            'namaPengguna' => 'required|max:50|unique:login,username',
            'email'        => 'required|max:255',
            'noTelp'       => 'required|max:15',
            'tanggalLahir' => 'required|date|date_format:Y-m-d',
            'alamat'       => 'required',
            'kataSandi'    => 'required|min:6',
            'kataSandi'    => 'required|min:6|required_with:kataSandi|same:kataSandi'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back();
        endif;

        try {
            $bcrypt = bcrypt($pwd);
            $profilelogin = ProfileLogin::create([
                'nama_lengkap' => $fullname,
                'alamat'       => $address,
                'ttl'          => $borndate,
                'telp'         => $notelp
            ]);
            $login = User::create([
                'username'  => $username,
                'password'  => $bcrypt,
                'id_role'   => 4,
                'id_profil' => $profilelogin->id_profil
            ]);
            $profile->move(public_path().'/upload/profile/user/', $login->id_login.'.jpg');
            AdminDisdukcapil::create([
                'id_login' => $login->id_login,
                'email'    => $email,
                'status'   => 2
            ]);
            
            alert()->success('Menambahkan akun admin telah berhasil');
            return back();
        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }
    }

    public function changeimage(Request $request)
    {
        $pk  = $request->pk;
        $img = $request->file('file');

        $validator = Validator::make($request->all(), [
            'pk' => 'required',
            'file' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $img->move(public_path().'/upload/profile/user/', $pk.'.jpg');

        alert()->success('Gambar profil berhasil di ganti');
        return back();
    }


    public function changepwd(Request $request)
    {
        $pk       = $request->pk;
        $newpwd   = $request->kataSandiBaru;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $user     = User::where('id_login', $idlogin)->first();
        $validator = Validator::make($request->all(), [
            'kataSandiBaru'        => 'required|max:255',
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        if(password_verify($pwdlogin, $user->password)):
            $bcrypt = bcrypt($newpwd);
            User::where('id_login', $idlogin)->update([
                'password' => $bcrypt
            ]);

            alert()->success('Ubah kata sandi telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kemabli');
            return back();
        endif;
    }

    public function destroydata(Request $request)
    {
        $pk       = $request->pk;
        $pwdlogin = $request->kataSandiSedangLogin;
        $idlogin  = Session::get('idlogin');
        $validator = Validator::make($request->all(), [
            'kataSandiSedangLogin' => 'required',
            'pk'                   => 'required'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $user  = User::where('id_login', $idlogin)->first();
        if(password_verify($pwdlogin, $user->password)):
            AdminDisdukcapil::where('id_login', $pk)->update([
                'status' => 3
            ]);

            alert()->success('Hapus akun telah berhasil');
            return back();
        else: 
            alert()->error('Kata sandi yang sedang login salah dan tidak sesuai silahkan coba kembali');
            return back();
        endif;

    }
}
