<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Pernikahan;
use App\Models\AdminVihara;
use App\Models\Pandita;
use App\Models\Vihara;
use Carbon\Carbon;
use App\Models\User;
use Response;
use Exception;
use File;

class PernikahanController extends Controller
{
    public function index(Request $request)
    {
        $nmpandita = $request->namaPandita;
        $nmsuami = $request->namaSuami;
        $nmistri = $request->namaIstri;
        $mindate = $request->minDate;
        $maxdate = $request->maxDate;
        $idlogin = Session::get('idlogin');
        $idrole  = Session::get('id_role');
        if($idrole == 1 || $idrole == 4):  
            $pandita = Pandita::join('profil_login', 'profil_login.id_profil', '=', 'pandita.id_profil')
                    ->get();  
            $pernikahan = Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita');
        else:
            if($idrole == 3): 
                $admin   = AdminVihara::where('id_login', $idlogin)->first();
            else: 
                $login = User::where('id_login', $idlogin)->first();
                $admin = Pandita::where('id_profil', $login->id_profil)->first();
            endif;
            $pandita = Pandita::join('profil_login', 'profil_login.id_profil', '=', 'pandita.id_profil')
                       ->where('id_vihara', $admin->id_vihara)
                       ->get();
    
            $pernikahan = Pernikahan::join('pandita', 'pandita.id_pandita', '=', 'pernikahan.id_pandita')
                          ->where('pernikahan.id_vihara', $admin->id_vihara);
        endif;
        $vihara = Vihara::where('status', 1)->get();

        if($nmpandita != NULL && $nmsuami != NULL && $nmistri != NULL && $mindate != NULL && $maxdate != NULL): 
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmsuami != NULL && $nmistri != NULL && $mindate != NULL):
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmsuami != NULL && $nmistri != NULL && $maxdate != NULL):
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmsuami != NULL && $mindate != NULL && $maxdate != NULL): 
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmpandita != NULL && $nmistri != NULL && $mindate != NULL && $maxdate != NULL): 
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmsuami != NULL && $nmistri != NULL && $mindate != NULL && $maxdate != NULL):
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));  
        elseif($nmpandita != NULL && $nmsuami != NULL && $nmistri != NULL):  
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmsuami != NULL && $mindate != NULL):  
            if(is_numeric($nmsuami) ):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmistri != NULL && $mindate != NULL): 
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmsuami != NULL && $nmistri != NULL && $mindate != NULL): 
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmpandita != NULL && $nmsuami != NULL && $maxdate != NULL):  
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmistri != NULL && $maxdate != NULL):
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmsuami != NULL && $nmistri != NULL && $maxdate != NULL): 
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmpandita != NULL && $mindate != NULL && $maxdate != NULL): 
            $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmsuami != NULL && $mindate != NULL && $maxdate != NULL):  
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmistri != NULL && $mindate != NULL && $maxdate != NULL):  
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_istri', $nmistri)
                         ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $nmsuami != NULL): 
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_suami', $nmsuami)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmpandita != NULL && $nmistri != NULL):  
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->where('nik_istri', $nmistri)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $mindate != NULL):  
            $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);

        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL && $maxdate != NULL): 
            $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                    ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                    ->orderBy('tgl_nikah','DESC')
                    ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmsuami != NULL && $nmistri != NULL):
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('nik_istri', $nmistri)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            elseif(!is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nik_istri', $nmistri)
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            elseif(!is_numeric($nmsuami) && !is_numeric($nmistri)):
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));  
        elseif($nmsuami != NULL && $mindate != NULL): 
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmsuami != NULL && $maxdate != NULL): 
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->where('tgl_nikah', '<=',$maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara')); 
        elseif($nmistri != NULL && $mindate != NULL):  
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmistri != NULL && $maxdate != NULL):  
            if(is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_istri', $nmistri)
                         ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($mindate != NULL && $maxdate != NULL):  
            $nikah = $pernikahan->whereBetween('tgl_nikah', [$mindate.' 00:00:00', $maxdate.' 23:59:59'])
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmpandita != NULL):  
                $nikah = $pernikahan->where('pernikahan.id_pandita', $nmpandita)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);

        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmsuami != NULL): 
            if(is_numeric($nmsuami)):
                $nikah = $pernikahan->where('nik_suami', $nmsuami)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_suami', 'LIKE', '%'.$nmsuami.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($nmistri != NULL):  
            if(is_numeric($nmsuami) && is_numeric($nmistri)):
                $nikah = $pernikahan->where('nik_istri', $nmistri)
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
            else:
                $nikah = $pernikahan->where('nama_istri', 'LIKE', '%'.$nmistri.'%')
                        ->orderBy('tgl_nikah','DESC')
                        ->paginate(20);
            endif;
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($mindate != NULL):  
                $nikah = $pernikahan->where('tgl_nikah', '>=', $mindate.' 00:00:00')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        elseif($maxdate != NULL):  
                $nikah = $pernikahan->where('tgl_nikah', '<=', $maxdate.' 23:59:59')
                         ->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        else: 
            $nikah = $pernikahan->orderBy('tgl_nikah','DESC')
                         ->paginate(20);
        
            return view('pages.pernikahan', compact('nikah', 'nmpandita', 'nmsuami', 'nmistri', 'mindate', 'maxdate', 'pandita', 'idrole', 'vihara'));
        endif;


    }

    public function inputpage()
    {
        
        $idlogin = Session::get('idlogin');
        $idrole  = Session::get('id_role');

        if($idrole == 1):
            $pandita = Pandita::join('profil_login', 'profil_login.id_profil', '=', 'pandita.id_profil')
                        ->get();
        else:
            $admin   = AdminVihara::where('id_login', $idlogin)->first();
            $pandita = Pandita::join('profil_login', 'profil_login.id_profil', '=', 'pandita.id_profil')
                       ->where('id_vihara', $admin->id_vihara)
                       ->get();
        endif;
        $vihara  = Vihara::where('status', 1)->get();
        return view('pages.inputpernikahan', compact('pandita', 'idlogin', 'vihara', 'idrole'));
    }

    public function ubahakte(Request $request)
    {
        $idrole = Session::get('id_role');
        $filename = $request->filename; 
        $file     = $request->aktePerkawinan;
        if($idrole != 4):
            alert()->error('Mohon maaf tidak bisa mengakses ke halaman tujuan');
            return back();
        endif;

        $validator = Validator::make($request->all(), [
            'filename'       => 'required',
            'aktePerkawinan' => 'required|mimes:jpg'
        ]);

        if($validator->fails()):
            alert()->error($validator->errors()->all());
            return back(); 
        endif;
        $file->move(public_path().'/disdukcapil/', $filename.'.jpg');

        alert()->success('Ubah Akte Pernikahan Berhasil');
        return back();
        
    }

    public function insert(Request $request)
    {
        $bukunikah              = $request->file('suratKeteranganKawin');
        $nonikah                = $request->noKeteranganKawin;
        $namapandita            = $request->namaPandita;
        $namasuami              = $request->namaSuami;
        $fotosuami              = $request->file('fotoSuami3x4');
        $suratijinpolritnisuami = $request->file('suratIjinTniPolriSuami');
        $aktekelahiransuami     = $request->file('akteKelahiranSuami');
        $niksuami               = $request->nikSuami;
        $tgllhrsuami            = $request->tglLhrSuami;
        $ktpsuami               = $request->file('ktpSuami');
        $kksuami                = $request->file('kkSuami');
        $aktaperceraiansuami    = $request->file('fotoAktePerceraianSuami');
        $aktakematianistri      = $request->file('fotoAkteKematianIstri');
        $namaistri              = $request->namaIstri;
        $fotoistri              = $request->file('fotoIstri3x4');
        $suratijinpolritniistri = $request->file('suratIjinTniPolriIstri');
        $aktekelahiranistri     = $request->file('akteKelahiranIstri');
        $nikistri               = $request->nikIstri;
        $tgllhristri            = $request->tglLhrIstri;
        $ktpistri               = $request->file('ktpIstri');
        $kkistri                = $request->file('kkIstri');
        $aktaperceraianistri    = $request->file('fotoAktePerceraianIstri');
        $aktakematiansuami      = $request->file('fotoAkteKematianSuami');
        $tglnikah               = $request->tglNikah;
        $vihara                 = $request->vihara;
        $idlogin                = Session::get('idlogin');
        $idrole                 = Session::get('id_role');
        $userlogin              = AdminVihara::where('id_login', $idlogin)
                              ->first();
        $pernikahan = Pernikahan::select('id_nikah')
                       ->orderBy('id_nikah', 'desc')
                       ->first();
        if($pernikahan):
            $IdGenerateNk = idgenerate($pernikahan->id_nikah, 'NK');
        else: 
            $IdGenerateNk = idgenerate('', 'NK', '', '', ''); 
        endif;
        if($idrole == 1): 
            $validator = Validator::make($request->all(), [
                'noKeteranganKawin'    => 'required|unique:pernikahan,no_nikah',
                'namaPandita'          => 'required|max:5',
                'namaSuami'            => 'required|max:255',
                'nikSuami'             => 'required|max:20',
                'tglLhrSuami'          => 'required',
                'ktpSuami'             => 'required|mimes:jpg',
                'kkSuami'              => 'required|mimes:jpg',
                'kkIstri'              => 'required|mimes:jpg',
                'fotoSuami3x4'         => 'required|mimes:jpg',
                'fotoIstri3x4'         => 'required|mimes:jpg',
                'akteKelahiranSuami'   => 'required|mimes:jpg',
                'akteKelahiranIstri'   => 'required|mimes:jpg',
                'namaIstri'            => 'required|max:255',
                'nikIstri'             => 'required|max:20',
                'tglLhrIstri'          => 'required',
                'ktpIstri'             => 'required|mimes:jpg',
                'tglNikah'             => 'required',
                'suratKeteranganKawin' => 'required|mimes:jpg',
                'vihara'               => 'required'
            ]);
    
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;
        else: 
            $validator = Validator::make($request->all(), [
                'noKeteranganKawin'              => 'required|unique:pernikahan,no_nikah',
                'namaPandita'          => 'required|max:5',
                'namaSuami'            => 'required|max:255',
                'nikSuami'             => 'required|max:20',
                'tglLhrSuami'          => 'required',
                'ktpSuami'             => 'required|mimes:jpg',
                'kkSuami'              => 'required|mimes:jpg',
                'kkIstri'              => 'required|mimes:jpg',
                'fotoSuami3x4'         => 'required|mimes:jpg',
                'fotoIstri3x4'         => 'required|mimes:jpg',
                'akteKelahiranSuami'   => 'required|mimes:jpg',
                'akteKelahiranIstri'   => 'required|mimes:jpg',
                'ktpSuami'             => 'required|mimes:jpg',
                'namaIstri'            => 'required|max:255',
                'nikIstri'             => 'required|max:20',
                'tglLhrIstri'          => 'required',
                'ktpIstri'             => 'required|mimes:jpg',
                'tglNikah'             => 'required',
                'suratKeteranganKawin' => 'required|mimes:jpg'
            ]);
    
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;
        endif;
        
        try {
            if($idrole == 1): 
                
                $pernikahan = Pernikahan::create([
                    'id_nikah'        => $IdGenerateNk,
                    'no_nikah'        => $nonikah,
                    'id_vihara'       => $vihara,
                    'id_pandita'      => $namapandita,
                    'id_login'        => $idlogin,
                    'nama_suami'      => $namasuami,
                    'nik_suami'       => $niksuami,
                    'tgl_lahir_suami' => $tgllhrsuami,
                    'nama_istri'      => $namasuami,
                    'nik_istri'       => $nikistri,
                    'tgl_lahir_istri' => $tgllhristri,
                    'tgl_nikah'       => $tglnikah,
                    'type'            => 1,
                ]);
            else: 
                $pernikahan = Pernikahan::create([
                    'id_nikah'        => $IdGenerateNk,
                    'no_nikah'        => $nonikah,
                    'id_vihara'       => $userlogin->id_vihara,
                    'id_pandita'      => $namapandita,
                    'id_login'        => $idlogin,
                    'nama_suami'      => $namasuami,
                    'nik_suami'       => $niksuami,
                    'tgl_lahir_suami' => $tgllhrsuami,
                    'nama_istri'      => $namasuami,
                    'nik_istri'       => $nikistri,
                    'tgl_lahir_istri' => $tgllhristri,
                    'tgl_nikah'       => $tglnikah,
                    'type'            => 1,
                ]);
            endif;
            $bukunikah->move(public_path().'/pernikahan/suratketerangannikah/', $pernikahan->id_nikah.'.jpg');
            // ---- Suami ---- //
            $ktpsuami->move(public_path().'/pernikahan/suami/ktp/', $pernikahan->id_nikah.'.jpg');
            $fotosuami->move(public_path().'/pernikahan/suami/foto3x4/', $pernikahan->id_nikah.'.jpg');
            $aktekelahiransuami->move(public_path().'/pernikahan/suami/aktekelahiran/', $pernikahan->id_nikah.'.jpg');
            $kksuami->move(public_path().'/pernikahan/suami/kk/', $pernikahan->id_nikah.'.jpg');
            if($aktaperceraiansuami):
                $aktaperceraiansuami->move(public_path().'/pernikahan/suami/akteperceraian/', $pernikahan->id_nikah.'.jpg');
            endif;
            if($aktakematianistri): 
                $aktakematianistri->move(public_path().'/pernikahan/suami/aktekematian/', $pernikahan->id_nikah.'.jpg');
            endif;

            if($suratijinpolritnisuami): 
                $suratijinpolritnisuami->move(public_path().'/pernikahan/suami/suratijintnipolri/', $pernikahan->id_nikah.'.jpg');
            endif;

            // ---- Istri ---- //
            $ktpistri->move(public_path().'/pernikahan/istri/ktp/', $pernikahan->id_nikah.'.jpg');
            $fotoistri->move(public_path().'/pernikahan/istri/foto3x4/', $pernikahan->id_nikah.'.jpg');
            $aktekelahiranistri->move(public_path().'/pernikahan/istri/aktekelahiran/', $pernikahan->id_nikah.'.jpg');
            $kkistri->move(public_path().'/pernikahan/istri/kk/', $pernikahan->id_nikah.'.jpg');
            if($aktaperceraianistri): 
                $aktaperceraianistri->move(public_path().'/pernikahan/istri/akteperceraian/', $pernikahan->id_nikah.'.jpg');
            endif;
            if($aktakematiansuami):
                $aktakematiansuami->move(public_path().'/pernikahan/istri/aktekematian/', $pernikahan->id_nikah.'.jpg');
            endif;

            if($suratijinpolritniistri): 
                $suratijinpolritniistri->move(public_path().'/pernikahan/suami/suratijintnipolri/', $pernikahan->id_nikah.'.jpg');
            endif;

            return redirect()->route('Pernikahan')->with('success', 'Menambahkan data telah berhasil');
        } catch(Exception $e) {
            alert()->error($e->getMessage());
            return back();
        }

    }

    public function pandita(Request $request)
    {
        $id_vihara = $request->idvihara;

        $pandita = Pandita::join('profil_login', 'profil_login.id_profil', '=', 'pandita.id_profil')->where('id_vihara', $id_vihara)->get();

        return response()->json([
            "pandita" => $pandita
       ]);

    }

    public function update(Request $request)
    {
        $bukunikah              = $request->file('suratKeteranganKawin');
        $nonikah                = $request->noKeteranganKawin;
        $niksuami               = $request->nikSuami;
        $namasuami              = $request->namaSuami;
        $tgllhrsuami            = $request->tanggalLahirSuami;
        $ktpsuami               = $request->file('ktpSuami');
        $kksuami                = $request->file('kkSuami');
        $fotosuami              = $request->file('fotoSuami3x4');
        $aktekelahiransuami     = $request->file('fotoAkteKelahiranSuami');
        $akteperceraiansuami    = $request->file('fotoAktePerceraianSuami');
        $aktekematianistri      = $request->file('fotoAkteKematianIstri');
        $suratijinpolritnisuami = $request->file('suratIjinTniPolriSuami');
        $nikistri               = $request->nikIstri;
        $namaistri              = $request->namaIstri;
        $tgllhristri            = $request->tanggalLahirIstri;
        $namapandita            = $request->namaPandita;
        $tglnikah               = $request->tanggalNikah;
        $jamNikah               = $request->jamNikah;
        $ktpistri               = $request->file('ktpIstri');
        $kkistri                = $request->file('kkIstri');
        $fotoistri              = $request->file('fotoIstri3x4');
        $aktekelahiranistri     = $request->file('fotoAkteKelahiranIstri');
        $akteperceraianistri    = $request->file('fotoAktePerceraianIstri');
        $aktekematiansuami      = $request->file('fotoAkteKematianSuami');
        $suratijinpolritniistri = $request->file('suratIjinTniPolriIstri');
        $idnikah                = $request->id_nikah;
        $vihara                 = $request->vihara;
        $idlogin                = Session::get('idlogin');
        $idrole                 = Session::get('id_role');

        
        if($bukunikah):
            $img = Validator::make($request->all(), [
                'suratKeteranganKawin' => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        // --- Suami ---- //
        if($ktpsuami):
            $img = Validator::make($request->all(), [
                'ktpSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        if($kksuami): 
            $img = Validator::make($request->all(), [
                'kkSuami'  => 'required|mimes:jpg'
            ]);  
           
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
        if($fotosuami):
            $img = Validator::make($request->all(), [
                'fotoSuami3x4'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekelahiransuami): 
            $img = Validator::make($request->all(), [
                'fotoAkteKelahiranSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($akteperceraiansuami): 
            $img = Validator::make($request->all(), [
                'fotoAktePerceraianSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekematianistri): 
            $img = Validator::make($request->all(), [
                'fotoAkteKematianIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($suratijinpolritnisuami): 
            $img = Validator::make($request->all(), [
                'suratIjinTniPolriSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        // ---- Istri ---- //
        if($ktpistri):
            $img = Validator::make($request->all(), [
                'ktpIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($kkistri): 
            $img = Validator::make($request->all(), [
                'kkIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($fotoistri):
            $img = Validator::make($request->all(), [
                'fotoIstri3x4'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekelahiranistri): 
            $img = Validator::make($request->all(), [
                'fotoAkteKelahiranIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($akteperceraianistri): 
            $img = Validator::make($request->all(), [
                'fotoAktePerceraianIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;

        if($aktekematiansuami): 
            $img = Validator::make($request->all(), [
                'fotoAkteKematianSuami'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
        if($suratijinpolritniistri): 
            $img = Validator::make($request->all(), [
                'suratIjinTniPolriIstri'  => 'required|mimes:jpg'
            ]);  
            
            if($img->fails()):
                alert()->error($img->errors()->all());
                return back(); 
            endif;
        endif;
        
        if($idrole == 1):
            
            $validator = Validator::make($request->all(), [
                'id_nikah'          => 'required',
                'noKeteranganKawin'           => 'required',
                'namaPandita'       => 'required|max:5',
                'namaSuami'         => 'required|max:255',
                'nikSuami'          => 'required|max:20',
                'tanggalLahirSuami' => 'required',
                'namaIstri'         => 'required|max:255',
                'nikIstri'          => 'required|max:20',
                'tanggalLahirIstri' => 'required',
                'tanggalNikah'      => 'required',
                'jamNikah'          => 'required',
                'vihara'            => 'required'
            ]);

            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;

            try {            
                $pernikahan = Pernikahan::where('id_nikah', $idnikah)->update([
                    'noKeteranganKawin' => $nonikah,
                    'id_vihara'         => $vihara,
                    'id_pandita'        => $namapandita,
                    'id_login'          => $idlogin,
                    'nama_suami'        => $namasuami,
                    'nik_suami'         => $niksuami,
                    'tgl_lahir_suami'   => $tgllhrsuami,
                    'nama_istri'        => $namasuami,
                    'nik_istri'         => $nikistri,
                    'tgl_lahir_istri'   => $tgllhristri,
                    'tgl_nikah'         => $tglnikah.' '.$jamNikah
                ]);

            } catch(Exception $e) {
                alert()->error($e->getMessage());
                return back();
            }
        else: 
            $userlogin   = AdminVihara::where('id_login', $idlogin)
                        ->first();
            
            
            $validator = Validator::make($request->all(), [
                'id_nikah'          => 'required',
                'noKeteranganKawin' => 'required',
                'namaPandita'       => 'required|max:5',
                'namaSuami'         => 'required|max:255',
                'nikSuami'          => 'required|max:20',
                'tanggalLahirSuami' => 'required',
                'namaIstri'         => 'required|max:255',
                'nikIstri'          => 'required|max:20',
                'tanggalLahirIstri' => 'required',
                'tanggalNikah'      => 'required',
                'jamNikah'          => 'required'
            ]);
            
            if($validator->fails()): 
                alert()->error($validator->errors()->all());
                return back();
            endif;

            try {   
                        
                $pernikahan = Pernikahan::where('id_nikah', $idnikah)->update([
                    'no_nikah'        => $nonikah,
                    'id_vihara'       => $userlogin->id_vihara,
                    'id_pandita'      => $namapandita,
                    'id_login'        => $idlogin,
                    'nama_suami'      => $namasuami,
                    'nik_suami'       => $niksuami,
                    'tgl_lahir_suami' => $tgllhrsuami,
                    'nama_istri'      => $namasuami,
                    'nik_istri'       => $nikistri,
                    'tgl_lahir_istri' => $tgllhristri,
                    'tgl_nikah'       => $tglnikah.' '.$jamNikah
                ]);
                

                if($bukunikah):
                    $bukunikah->move(public_path().'/pernikahan/bukupernikahan/', $idnikah.'.jpg');
                endif;
                // --- Suami ---- //
                if($ktpsuami):
                    $ktpsuami->move(public_path().'/pernikahan/suami/ktp/', $idnikah.'.jpg');
                endif;
    
                if($kksuami): 
                    $kksuami->move(public_path().'/pernikahan/suami/kk/', $idnikah.'.jpg');
                endif;
    
                if($fotosuami):
                    $fotosuami->move(public_path().'/pernikahan/suami/foto3x4/', $idnikah.'.jpg'); 
                endif;
    
                if($aktekelahiransuami): 
                    $aktekelahiransuami->move(public_path().'/pernikahan/suami/aktekelahiran/', $idnikah.'.jpg');
                endif;
    
                if($akteperceraiansuami): 
                    $akteperceraiansuami->move(public_path().'/pernikahan/suami/akteperceraian/', $idnikah.'.jpg');
                endif;
    
                if($aktekematianistri): 
                    $aktekematianistri->move(public_path().'/pernikahan/suami/aktekematian/', $idnikah.'.jpg');
                endif;
    
                if($suratijinpolritnisuami): 
                    $suratijinpolritnisuami->move(public_path().'/pernikahan/suami/suratijintnipolri/', $idnikah.'.jpg');
                endif;
    
                // ---- Istri ---- //
                if($ktpistri):
                    $ktpistri->move(public_path().'/pernikahan/istri/ktp/', $idnikah.'.jpg');
                endif;
    
                if($kkistri): 
                    $kkistri->move(public_path().'/pernikahan/istri/kk/', $idnikah.'.jpg');
                endif;
    
                if($fotoistri):
                    $fotoistri->move(public_path().'/pernikahan/istri/foto3x4/', $idnikah.'.jpg'); 
                endif;
    
                if($aktekelahiranistri): 
                    $aktekelahiranistri->move(public_path().'/pernikahan/istri/aktekelahiran/', $idnikah.'.jpg');
                endif;
    
                if($akteperceraianistri): 
                    $akteperceraianistri->move(public_path().'/pernikahan/istri/akteperceraian/', $idnikah.'.jpg');
                endif;
    
                if($aktekematiansuami): 
                    $aktekematiansuami->move(public_path().'/pernikahan/istri/aktekematian/', $idnikah.'.jpg');
                endif;
    
                if($suratijinpolritniistri): 
                    $suratijinpolritniistri->move(public_path().'/pernikahan/istri/suratijintnipolri/', $idnikah.'.jpg');
                endif;

            } catch(Exception $e) {
                alert()->error($e->getMessage());
                return back();
            }
        endif;


       

        alert()->success('Edit data telah berhasil');
        return back();
    }

    public function destroy(Request $request)
    {
        $pk = $request->pk;

        $validator = Validator::make($request->all(), [
            'pk' => 'required|max:25'
        ]);

        if($validator->fails()): 
            alert()->error($validator->errors()->all());
            return back();
        endif;

        $pernikahan = Pernikahan::where('id_nikah', $pk)->first();
        if(!$pernikahan):
            alert()->error('Mohon maaf data yang mau dihapus tidak ditemukan');
            return back(); 
        endif;

        Pernikahan::where('id_nikah', $pk)->delete();
        $bukupernikahan = public_path().'/pernikahan/bukupernikahan/'.$pk.'.jpg';
        File::delete($bukupernikahan);

        // --- Istri --- //
        $ktpistri = public_path().'/pernikahan/istri/ktp/'.$pk.'.jpg';
        if(file_exists($ktpistri)):
            File::delete($ktpistri);
        endif;

        // --- Suami --- //
        $ktpsuami = public_path().'/pernikahan/suami/ktp/'.$pk.'.jpg';
        if(file_exists($ktpsuami)): 
            File::delete($ktpsuami);
        endif;

        $kksuami = public_path().'/pernikahan/suami/kk/'.$pk.'.jpg';
        if(file_exists($kksuami)):
            File::delete($kksuami); 
        endif;

        $fotosuami = public_path().'/pernikahan/suami/foto3x4/'.$pk.'.jpg';
        if(file_exists($fotosuami)):
            File::delete($fotosuami); 
        endif;

        $aktekelahiransuami = public_path().'/pernikahan/suami/aktekelahiran/'.$pk.'.jpg';
        if(file_exists($aktekelahiransuami)):
            File::delete($aktekelahiransuami); 
        endif;

        $akteperceraiansuami = public_path().'/pernikahan/suami/akteperceraian/'.$pk.'.jpg';
        if(file_exists($akteperceraiansuami)):
            File::delete($akteperceraiansuami); 
        endif;

        $aktekematiansuami = public_path().'/pernikahan/suami/aktekematian/'.$pk.'.jpg';
        if(file_exists($aktekematiansuami)):
            File::delete($aktekematiansuami); 
        endif;



        alert()->success('Hapus data berhasil');
        return back();
    }
}
