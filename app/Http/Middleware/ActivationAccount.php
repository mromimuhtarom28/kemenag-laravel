<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\AdminVihara;
use Session;


class ActivationAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $login   = Session::get('login');
        $idlogin = Session::get('idlogin');
        $role    = Session::get('id_role');
        if($role == 3): 
            $AdminVihara = AdminVihara::join('profil_login', 'profil_login.id_profil', '=', 'admin_vihara.id_profil')
                            ->join('login', 'admin_vihara.id_login', 'login.id_login')
                            ->where('admin_vihara.id_login', $idlogin)
                            ->first();
            if($AdminVihara->status == 0): 
                return redirect()->route('Profile');
            elseif($AdminVihara->status == 1):
                alert()->error('Menunggu diterima dari admin kementrian agama');
                return back();
            else: 
                return $next($request);
            endif;
        else:
            return $next($request);
        endif;
    }
}
